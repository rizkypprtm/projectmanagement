﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjectManagement.Entities;
using ProjectManagement.Models;
using PagedList;

//Author : Roihatul Jannah
//Created Date : 22-11-2018



namespace ProjectManagement.Controllers
{
    public class ChangeRequestController : Controller
    {
        private ProjectManagementDBContext db = new ProjectManagementDBContext();

        // GET: ChangeRequests
        public ActionResult Index()
        {
            //var changeRequests = db.ChangeRequests.Include(c => c.Requirement);
            return View();
        }

        public ActionResult Edit(int? id)
        {
            ViewBag.ChangeRequestId = id;
            //ViewBag.DocumentId = id;
            ChangeRequest changeRe = db.ChangeRequests.Find(id);
            ViewBag.DocumentTypeId = new SelectList(db.DocumentTypes, "DocumentTypeId", "Description");
            return View(changeRe);
        }

        public ActionResult EditChangeRequest(
            int ChangeRequestId, 
            int RequimentId,
            string ChangeRequestNumber,
            string ChangeRequestDate,
            string Description,
            string MandaysEstimation)
        {
            var result = new jsonMessage();
            try
            {
                ChangeRequest changeRe = db.ChangeRequests.Find(ChangeRequestId);
                changeRe.RequimentId = RequimentId;
                changeRe.ChangeRequestNumber = ChangeRequestNumber;
                changeRe.Description = Description;
                if (ChangeRequestDate != "")
                {
                    changeRe.ChangeRequestDate = Convert.ToDateTime(ChangeRequestDate);
                }
                if (MandaysEstimation != "")
                {
                    changeRe.MandaysEstimation = Convert.ToDecimal(MandaysEstimation);
                }
                result.Message = "Ubah Berhasil";
                result.Status = true;
                db.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                result.Message = e + "Permintaan Anda tidak bisa diproses. Coba lagi nanti.";
                result.Status = false;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
            //return Redirect("EditRe");
        }

        public ActionResult EditChangeRequestDoc(
            int DocumentId,
            int ChangeRequestId,
            string DocumentTypeId,
            string DocumentName,
            string FilePath,
            string Version)
        {
            var result = new jsonMessage();
            try
            {
                ChangeRequestDocument changeReDoc = db.ChangeRequestDocuments.Find(DocumentId);
                changeReDoc.ChangeRequestId = ChangeRequestId;
                changeReDoc.DocumentName = DocumentName;
                changeReDoc.FilePath = FilePath;
                changeReDoc.Version = Version;
                if (DocumentTypeId != "")
                {
                    changeReDoc.DocumentTypeId = Convert.ToInt32(DocumentTypeId);
                }
                result.Message = "Ubah Berhasil";
                result.Status = true;
                db.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                result.Message = e + "Permintaan Anda tidak bisa diproses. Coba lagi nanti.";
                result.Status = false;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
            //return Redirect("EditRe");
        }
        public JsonResult GetDataById(int? id)
        {
            var data = db.ChangeRequests.Find(id);
            ChangeRequesViewModel changeRe = new ChangeRequesViewModel()
            {
                RequimentId = data.RequimentId,
                reqnumber = data.Requirement.RequirementNumber,
                ProjectName = data.Requirement.Project.ProjectName,
                ModulName = data.Requirement.Modul.ModulName,
                ChangeRequestNumber = data.ChangeRequestNumber,
                StrChangeRequestDate = data.ChangeRequestDate.ToString(),
                MandaysEstimation = data.MandaysEstimation,
                Description = data.Description
            };
            return Json(changeRe, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDataCRDById(int? id)
        {
            //var crd = db.ChangeRequestDocuments.Where(r => r.ChangeRequestId == id).Select(s => s.DocumentId).FirstOrDefault();
            var data = db.ChangeRequestDocuments.Find(id);
            ChangeRequestDocumentViewModel changeReDoc = new ChangeRequestDocumentViewModel()
            {
                DocumentId = data.DocumentId,
                ChangeRequestId = data.ChangeRequestId,
                DocumentTypeId = data.DocumentTypeId,
                DocumentName = data.DocumentName,
                FilePath = data.FilePath,
                Version = data.Version
            };

            return Json(changeReDoc, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDataChangeReqDoc(int? id,int? page,string search=null)
        {
            if (page == null)
            {
                page = 1;
            }
            
            int pageSize = 5;
            int length = db.ChangeRequestDocuments.Where(c=>c.ChangeRequest.ChangeRequestId==id).ToList().Count();
            int pageNumber = (page ?? 1);
            var changeRequest = db.ChangeRequestDocuments.Where(z => z.ChangeRequestId == id).ToList();
            if (search != "")
            {
                changeRequest = changeRequest.Where(z => z.ChangeRequest.Requirement.Modul.ModulNumber.ToLower().Contains(search.ToLower()) || 
                z.ChangeRequest.Requirement.RequirementNumber.ToLower().Contains(search.ToLower()) || 
                z.ChangeRequest.Requirement.Description.ToLower().Contains(search.ToLower()) || 
                z.ChangeRequest.Requirement.Project.ProjectName.ToLower().Contains(search.ToLower()) || 
                z.ChangeRequest.Requirement.Modul.ModulName.ToLower().Contains(search.ToLower()) || 
                z.ChangeRequest.Description.ToLower().Contains(search.ToLower()) || 
                z.ChangeRequest.Requirement.Level.Equals(search)|| 
                z.ChangeRequest.Requirement.CreationBy.ToLower().Contains(search.ToLower()) ||
                z.ChangeRequest.Requirement.ModifiedBy.ToLower().Contains(search.ToLower())).ToList();
                length = changeRequest.Count();
            }
            var data = changeRequest.AsEnumerable()
                .OrderBy(o => o.DocumentId)
                .Select(c => new ChangeRequestDocumentViewModel()
                {
                    DocumentId = c.DocumentId,
                    ModulNumber = c.ChangeRequest.Requirement.Modul.ModulNumber,
                    RequirementNumber=c.ChangeRequest.Requirement.RequirementNumber,
                    RequirementDescription=c.ChangeRequest.Requirement.Description,
                    ProjectName=c.ChangeRequest.Requirement.Project.ProjectName,
                    ModulName=c.ChangeRequest.Requirement.Modul.ModulName,
                    Description=c.ChangeRequest.Description,
                    Level=c.ChangeRequest.Requirement.Level,
                    CreationBy=c.ChangeRequest.Requirement.CreationBy,
                    StrCreationDate=c.ChangeRequest.Requirement.CreationDate.ToString(),
                    ModifiedBy=c.ChangeRequest.Requirement.ModifiedBy,
                    StrModifiedDate=c.ChangeRequest.Requirement.ModifiedDate.ToString(),
                    lenght = length
                    //DocumentTypeId=c.DocumentTypeId,
                    //DocumentName=c.DocumentName,
                    //FilePath=c.FilePath,
                    //Version=c.Version
                }).ToPagedList(pageNumber, pageSize);

            
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}
