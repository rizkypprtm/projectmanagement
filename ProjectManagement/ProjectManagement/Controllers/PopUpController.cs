﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjectManagement.Entities;
using ProjectManagement.Models;

//Author             : Sahdan Hidayatul Muzaki
//Created date       : Kamis, 22/11/2018
//Modified by        : Sahdan Hidayatul Muzaki
//Date modified      : Jum'at, 23/11/2018
//Additional         : Method GetProjectByScheduleProject, Partial View
//Modified by        : Nispi Abdul Aziz
//Date modified      : Senin, 26/11/2018
//Additional         : Method SaveModul, SaveProject,PopUpAddModul, PopUpAddProject


namespace ProjectManagement.Controllers
{
    public class PopUpController : Controller
    {
        private ProjectManagementDBContext db = new ProjectManagementDBContext();
        // GET: Clients
        public ActionResult Index()
        {
            return View(db.ProjectSchedules.ToList());
        }

        public ActionResult PopUpProjectTimeline()
        {
            return View(db.ProjectTimelines.ToList());
        }

        public ActionResult PopUpAddModul()
        {
            Modul modul = db.Moduls.Find(1);
            ViewBag.ModulStatus = new SelectList(db.ModulStatus, "ModulStatusId", "Description");
            //ViewBag.ParentModulId = new SelectList(db.Moduls.Where(p => p.ProjectId == modul.ProjectId), "ModulId", "ModulName");
            ViewBag.ModulId = new SelectList(db.Moduls, "ModulId", "ModulName");
            //return View();
            return View(db.Projects.ToList());
        }

        public ActionResult PopUpAddRequirment()
        {
            Requirement req = db.Requirements.Find(1);
            // ViewBag.ModulStatus = new SelectList(db.ModulStatus, "ModulStatusId", "Description");
            //ViewBag.ParentModulId = new SelectList(db.Moduls.Where(p => p.ProjectId == modul.ProjectId), "ModulId", "ModulName");
            ViewBag.RequirmentTypeId = new SelectList(db.Requirements, "RequimentTypeId", "Description");
            //return View();
            return View(db.Requirements.ToList());
        }
        public JsonResult SaveModDoc(
            int idd,
            int id,
            string name,
            string file,
            string version)
        {
            var result = new jsonMessage();
            var Prodoc = db.ModulDocuments.Find(idd);
            Prodoc.DocumentTypeId = id;
            Prodoc.Version = version;
            Prodoc.DocumentName = name;
            Prodoc.FilePath = file;
            db.SaveChanges();
            result.Message = "UpdateBerhasil";
            return Json(result, JsonRequestBehavior.AllowGet);

        }
        public JsonResult SaveModul(
            int project,
            string ModulName,
            string ModulNumber,
            string ModulParent,
            string ModulPic,
            string ModulStatus,
            string maydays,
            string startDate,
            string EndDate
            )
        {
            var result = new jsonMessage();
            try
            {
                Modul mod = new Modul();
                mod.ProjectId = project;
                mod.ModulName = ModulName;
                mod.ModulNumber = ModulNumber;
                mod.ParentModulId = Convert.ToInt32(ModulParent);
                mod.PIC = ModulPic;
                mod.ModulStatusId = Convert.ToInt32(ModulStatus);
                mod.MadaysEstimation = Convert.ToDecimal(maydays);
                mod.StartDateActual = Convert.ToDateTime(startDate);
                mod.EndDateActual = Convert.ToDateTime(EndDate);
                //var z= db.Projects.Find(project);
                //z.StartDateActual = Convert.ToDateTime(startDate);
                db.Moduls.Add(mod);
                result.Message = "Data modul berhasil disimpan...";
                result.Status = true;
                db.SaveChanges();

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                result.Message = e + "Permintaan Anda tidak bisa diproses. Coba lagi nanti.";
                result.Status = false;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveProjectSchedule(int ProjectId, string Version, string StartDate, string EndDate, string CreatedDate, string CreatedBy)
        {
            var result = new jsonMessage();
            try
            {
                //define the model  
                ProjectSchedule newProjectSchedule = new ProjectSchedule();
                newProjectSchedule.ProjectScheduleId = 99;
                newProjectSchedule.ProjectId = ProjectId;
                newProjectSchedule.Version = Version;
                newProjectSchedule.StartDate = Convert.ToDateTime(StartDate);
                newProjectSchedule.EndDate = Convert.ToDateTime(EndDate);
                newProjectSchedule.CreatedDate = Convert.ToDateTime(CreatedDate);
                newProjectSchedule.CreatedBy = CreatedBy;
                newProjectSchedule.IsActive = true;


                //for insert recored..  
                db.ProjectSchedules.Add(newProjectSchedule);
                result.Message = "Data Schedule Project berhasil disimpan...";
                result.Status = true;

                db.SaveChanges();


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                //ErrorLogers.ErrorLog(ex);
                result.Message = ex + "\nPermintaan Anda tidak bisa diproses. Coba lagi nanti.";
                result.Status = false;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveProjectTimeline(int ProjectScheduleId, int ProjectId, int ModulId, string StartDate, string EndDate, int? stUpdate, int idPT)
        {
            var result = new jsonMessage();
            try
            {
                if (stUpdate == 1)
                {
                    ProjectTimeline ubah = db.ProjectTimelines.Find(idPT);
                    ubah.StartDate = Convert.ToDateTime(StartDate);
                    ubah.EndDate = Convert.ToDateTime(EndDate);

                    result.Message = "Data Project Timeline berhasil diubah...";
                }
                else
                {
                    //define the model  
                    ProjectTimeline newProjectTimeline = new ProjectTimeline();
                    newProjectTimeline.ProjectScheduleId = ProjectScheduleId;
                    newProjectTimeline.ProjectId = ProjectId;
                    newProjectTimeline.ModulId = ModulId;
                    newProjectTimeline.StartDate = Convert.ToDateTime(StartDate);
                    newProjectTimeline.EndDate = Convert.ToDateTime(EndDate);

                    //for insert recored..  
                    db.ProjectTimelines.Add(newProjectTimeline);
                    result.Message = "Data Project Timeline berhasil disimpan...";
                }
                
                result.Status = true;

                db.SaveChanges();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                //ErrorLogers.ErrorLog(ex);
                result.Message = ex + "\nPermintaan Anda tidak bisa diproses. Coba lagi nanti.";
                result.Status = false;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveChangeRequest(int RequimentId,
            string ChangeRequestNumber,
            string Description,
            string MandaysEstimation)
        {
            var result = new jsonMessage();
            try
            {
                ChangeRequest changeRe = new ChangeRequest();
                changeRe.RequimentId = RequimentId;
                changeRe.ChangeRequestNumber = ChangeRequestNumber;
                changeRe.Description = Description;
                if (MandaysEstimation != "")
                {
                    changeRe.MandaysEstimation = Convert.ToDecimal(MandaysEstimation);
                }
                db.ChangeRequests.Add(changeRe);
                result.Message = "Tambah Berhasil";
                result.Status = true;
                db.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                result.Message = e + "Permintaan Anda tidak bisa diproses. Coba lagi nanti.";
                result.Status = false;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveRequirement(string ProjectId,
            string ModulId,
            string RequimentTypeId,
            string RequirementNumber,
            string Description,
            int Level)
        {
            var result = new jsonMessage();
            try
            {
                Requirement dataReq = new Requirement();
                dataReq.ProjectId = Convert.ToInt32(ProjectId);
                dataReq.ModulId = Convert.ToInt32(ModulId);
                dataReq.RequimentTypeId = Convert.ToInt32(RequimentTypeId);
                dataReq.RequirementNumber = RequirementNumber;
                dataReq.Description = Description;
                dataReq.Level = Level;
                result.Message = "Tambah Berhasil";
                result.Status = true;
                db.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                result.Message = e + "Permintaan Anda tidak bisa diproses. Coba lagi nanti.";
                result.Status = false;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PopUpAddProject()
        {
            ViewBag.ProjectStatus = new SelectList(db.ProjectStatus, "ProjectStatusId", "Description");
            ViewBag.ClientId = new SelectList(db.Clients, "ClientId", "Name");

            return View();
        }

        public JsonResult SaveProject(
            string txtProjectName,
            string txtStartDatePlan,
            string txtDescription,
            string txtEndDatePlan,
            string clien,
            string txtStartDateActual,
            string txtEndDateAct,
            string txtPic,
            string status,
            string txtMadaysEstimation,
            string txtMandaysEstCall,
            string CreationBy,
            string CreationDate,
            string txtModifiedBy,
            string txtModifiedDate)
        {
            var result = new jsonMessage();
            try
            {
                var creationDate = DateTime.Parse(CreationDate);
                var startAct = DateTime.Parse(txtStartDateActual);
                var EndAct = DateTime.Parse(txtEndDateAct);
                var EndPlan = DateTime.Parse(txtEndDatePlan);
                var startPlan = DateTime.Parse(txtStartDatePlan);

                Project pro = new Project()
                {
                    CreationBy = CreationBy,
                    CreationDate = creationDate,
                    Description = txtDescription,
                    EndDateActual = EndAct,
                    EndDatePlan = EndPlan,
                    PIC = txtPic,
                    ProjectName = txtProjectName,
                    ProjectStatusId = Convert.ToInt32(status),
                    StartDateActual = startAct,
                    StartDatePlan = startPlan,
                    ClientId = Convert.ToInt32(clien)
                };
                if (txtMadaysEstimation != "")
                {
                    pro.MandaysEstimation = Convert.ToDecimal(txtMadaysEstimation);
                }
                if (txtMandaysEstCall != "")
                {
                    pro.MandaysEstimationCal = Convert.ToDecimal(txtMandaysEstCall);
                }

                db.Projects.Add(pro);
                result.Message = "Data Project berhasil disimpan...";
                result.Status = true;

                db.SaveChanges();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                //ErrorLogers.ErrorLog(ex);
                result.Message = ex + "Permintaan Anda tidak bisa diproses. Coba lagi nanti.";
                result.Status = false;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }



        public ActionResult GetProjectByScheduleProject()//int Id)
        {
            Project project = new Project();
            project = db.Projects.Find(8);

            Project project2 = new Project();

            return PartialView("GetProjectByScheduleProject", project); //INI MEMANGGIL FILE
        }



        public ActionResult PopUpModulDependency(int? idModul, int? idDependency)
        {
            idModul = 19;
          //  idDependency = 4;

            ViewBag.ModulId = idModul;
            ViewBag.ModulNumber = db.Moduls.Find(idModul).ModulNumber;
            ViewBag.ModulName = db.Moduls.Find(idModul).ModulName;
            ViewBag.DependencyId = null;

            if (idDependency != null)
            {
                ViewBag.DependencyId = idDependency;
            }
            ViewBag.ListModul = new SelectList(db.Moduls, "ModulId", "ModulName");

            return View();
        }

        public JsonResult SaveModulDependency(string DependencyId ,string ModulId, string DependToModul)
        {
            var result = new jsonMessage();
            try
            {
                if(DependencyId != null)
                {
                    var dataDep = db.ModulDependencies.Find(Convert.ToInt32(DependencyId));
                    dataDep.ModulId = Convert.ToInt32(ModulId);
                    dataDep.DependToModulId = Convert.ToInt32(DependToModul);
                }
                else
                {
                    ModulDependency newModDep = new ModulDependency()
                    {
                        ModulId = Convert.ToInt32(ModulId),
                        DependToModulId = Convert.ToInt32(DependToModul)
                    };

                    db.ModulDependencies.Add(newModDep);
                }
                result.Message = "Data Modul Dependency berhasil disimpan...";
                result.Status = true;

                db.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                result.Message = e + "Permintaan Anda tidak bisa diproses. Coba lagi nanti.";
                result.Status = false;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SaveChangeRequestDoc(int ChangeRequestId,
            int DocumentTypeId
            ,string DocumentName,
            string FilePath
            , string Version)
        {
            var result = new jsonMessage();
            ChangeRequestDocument chang = new ChangeRequestDocument()
            {
                ChangeRequestId=ChangeRequestId,
                DocumentTypeId=DocumentTypeId,
                DocumentName=DocumentName,
                FilePath=FilePath,
                Version=Version
            };
            db.ChangeRequestDocuments.Add(chang);
            db.SaveChanges();
            result.Message = "Tambah Berhasil";
            result.Status = true;

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}