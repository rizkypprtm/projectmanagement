﻿using PagedList;
using ProjectManagement.Entities;
using ProjectManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

// author       :Fauzi
// Created date : 22-11-2018
// Modified by  :Fauzi
// Date Modified: 05-12-2018
// Addtional    : edit GetDataProjectTimeLine,GetDataChangeRequest,GetDataRequirment

namespace ProjectManagement.Controllers
{
    public class EditModulController : Controller
    {
        private ProjectManagementDBContext db = new ProjectManagementDBContext();
        // GET: Projects/Edit/5 
        public ActionResult Edit(int? id,int? idDependency)
        {
            var modul = db.Moduls.Find(id);
            ViewBag.RequimentTypeId = new SelectList(db.RequimentTypes, "RequimentTypeId", "Description");
            ViewBag.ParentModulId = new SelectList(db.Moduls.Where(p => p.ProjectId == modul.ProjectId), "ModulId", "ModulName");
            ViewBag.ModulStatusId = new SelectList(db.ModulStatus, "ModulStatusId", "Description", modul.ModulStatusId);
            ViewBag.ProjectId = new SelectList(db.Projects, "ProjectId", "ProjectName", modul.ProjectId);
            ViewBag.ModulId = id;
            ViewBag.DocTypeId = new SelectList(db.DocumentTypes, "DocumentTypeId", "Description");
            ViewBag.ModulNumber = db.Moduls.Find(id).ModulNumber;
            ViewBag.ModulName = db.Moduls.Find(id).ModulName;
            if (idDependency != null)
            {
                var dataMD = db.ModulDependencies.Find(idDependency);
                db.ModulDependencies.Remove(dataMD);
                db.SaveChanges();
            }
            ViewBag.DocumentTypeId = new SelectList(db.DocumentTypes, "DocumentTypeId", "Description");
            ViewBag.ListModul = new SelectList(db.Moduls, "ModulId", "ModulName");
            return View();
        }
     

        // save Requirment
        public JsonResult SaveRequirement(string ProjectId,
         string ModulId,
         string RequimentTypeId,
         string RequirementNumber,
         string Description,
         string Level)
        {
            var result = new jsonMessage();
            try
            {
                Requirement dataReq = new Requirement();
                var dataProject = db.Projects.Find(Convert.ToInt32(ProjectId));
                dataReq.ProjectId = Convert.ToInt32(ProjectId);
                dataReq.ModulId = Convert.ToInt32(ModulId);
                dataReq.RequimentTypeId = Convert.ToInt32(RequimentTypeId);
                dataReq.RequirementNumber = RequirementNumber;
                dataReq.Description = Description;
                dataReq.Level = Convert.ToInt32(Level);
                dataReq.CreationBy = dataProject.CreationBy;
                dataReq.CreationDate = dataProject.CreationDate;
                dataReq.ModifiedBy = dataProject.ModifiedBy;
                dataReq.ModifiedDate = dataProject.ModifiedDate;
                result.Message = "Tambah Berhasil";
                result.Status = true;
                db.Requirements.Add(dataReq);
                db.SaveChanges();
                var dataRequirment = db.Requirements.
                    OrderByDescending(o => o.RequimentId)
                    .Select(
                    s => new RequirmentViewModel()
                    {
                        CreationBy = s.CreationBy,
                        ModifiedDate = s.ModifiedDate,
                        CreationDate = s.CreationDate,
                        Description = s.Description,
                        Level = s.Level,
                        ModifiedBy = s.ModifiedBy,
                        ModulId = s.ModulId,
                        ProjectId = s.ProjectId,
                        RequimentId= s.RequimentId,
                        RequirementNumber = s.RequirementNumber
                    }
                    ).FirstOrDefault();
                var dataCR = db.ChangeRequests.Where(p => p.RequimentId == dataRequirment.RequimentId).Select(s => new ChangeRequesViewModel()
                {
                    ChangeRequestId = s.ChangeRequestId,
                    ChangeRequestNumber = s.ChangeRequestNumber

                }).FirstOrDefault();
                RequirementHistory DataRH = new RequirementHistory()
                {
                    CreationBy = dataRequirment.CreationBy,
                    ModifiedDate = dataRequirment.ModifiedDate,
                    CreationDate = dataRequirment.CreationDate,
                    Description = dataRequirment.Description,
                    Level = dataRequirment.Level,
                    ModifiedBy = dataRequirment.ModifiedBy,
                    ModulId = dataRequirment.ModulId,
                    ProjectId = dataRequirment.ProjectId,
                    RequimentId = dataRequirment.RequimentId,
                    RequirementNumber = dataRequirment.RequirementNumber
                };
               // if(dataCR.ChangeRequestId!=null)
              //  DataRH.ChangeRequestId =48;
                db.RequirementHistories.Add(DataRH);
                db.SaveChanges();

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                result.Message = e + "Permintaan Anda tidak bisa diproses. Coba lagi nanti.";
                result.Status = false;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult FuncEditModDoc(
       string idd,
       string id,
       string name,
       string file,
       string version)
        {
            var x = Convert.ToInt32(idd);
            var idDTI = Convert.ToInt32(id);
            var result = new jsonMessage();
            var Prodoc = db.ModulDocuments.Find(x);
            Prodoc.DocumentTypeId = idDTI;
            if (version != "")
                Prodoc.Version = version;
            if (name != "")
                Prodoc.DocumentName = name;
            if (file != "")
                Prodoc.FilePath = file;
            db.SaveChanges();
            result.Message = "UpdateBerhasil";
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //Update Project Time Line
        public JsonResult SaveProjectTimeline(string ProjectId, string ModulId, string StartDate, string EndDate, string stUpdate, string idPT)
        {
            var result = new jsonMessage();
            try
            {
                var stUpdateI = Convert.ToInt32(stUpdate);
                var idPTI = Convert.ToInt32(idPT);
                var ProjectIdI = Convert.ToInt32(ProjectId);
                var ModulIdI = Convert.ToInt32(ModulId);
                if (stUpdateI == 1)
                {
                    ProjectTimeline ubah = db.ProjectTimelines.Find(idPTI);
                    ubah.StartDate = Convert.ToDateTime(StartDate);
                    ubah.EndDate = Convert.ToDateTime(EndDate);

                    result.Message = "Data Project Timeline berhasil diubah...";
                }
               
                result.Status = true;

                db.SaveChanges();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                //ErrorLogers.ErrorLog(ex);
                result.Message = ex + "\nPermintaan Anda tidak bisa diproses. Coba lagi nanti.";
                result.Status = false;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        // 
        public JsonResult EditChangeRequestDoc(
          string DocumentId,
          string ChangeRequestId,
          string DocumentTypeId,
          string DocumentName,
          string FilePath,
          string Version)
        {
            var result = new jsonMessage();
            try
            {
                var DocumentIdI = Convert.ToInt32(DocumentId);
                var ChangeRequestIdI = Convert.ToInt32(ChangeRequestId);
                ChangeRequestDocument changeReDoc = db.ChangeRequestDocuments.Find(DocumentIdI);
                changeReDoc.ChangeRequestId = ChangeRequestIdI;
                if(DocumentName!="")
                changeReDoc.DocumentName = DocumentName;
                if(FilePath!="")
                changeReDoc.FilePath = FilePath;
                if(Version!="")
                changeReDoc.Version = Version;
                if (DocumentTypeId != "")
                {
                    changeReDoc.DocumentTypeId = Convert.ToInt32(DocumentTypeId);
                }
                result.Message = "Ubah Berhasil";
                result.Status = true;
                db.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                result.Message = e + "Permintaan Anda tidak bisa diproses. Coba lagi nanti.";
                result.Status = false;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
            //return Redirect("EditRe");
        }
        //Get Data For Edit Change Requir mentDocument
        public JsonResult GetDataForEditChangeRequirmentDocument(int? id)
        {
            var dataCRD = db.ChangeRequestDocuments.Where(d => d.DocumentId== id).AsQueryable();
            

            var hasil = dataCRD.AsEnumerable().
                    OrderBy(d => d.DocumentName)
                    .Select(s => new ChangeRequestDocumentViewModel()
                    {
                        Version = s.Version,
                        RequirementNumber = s.ChangeRequest.Requirement.RequirementNumber,
                        DocumentName = s.DocumentName,
                        RequirementDescription = s.ChangeRequest.Requirement.Description,
                        ProjectName = s.ChangeRequest.Requirement.Project.ProjectName,
                        ModulName = s.ChangeRequest.Requirement.Modul.ModulName,
                        Description = s.ChangeRequest.Description,
                        Level = s.ChangeRequest.Requirement.Level,
                        ChangeRequestId = s.ChangeRequestId,
                        CreationBy = s.ChangeRequest.Requirement.CreationBy,
                        CreationDate = s.ChangeRequest.Requirement.CreationDate,
                        DocumentId = s.DocumentId,
                        DocumentTypeId = s.DocumentTypeId,
                        FilePath = s.FilePath,
                        ModifiedBy = s.ChangeRequest.Requirement.ModifiedBy,
                        ModifiedDate = s.ChangeRequest.Requirement.ModifiedDate,
                        ModulNumber = s.ChangeRequest.Requirement.Modul.ModulNumber,
                        StrCreationDate = s.ChangeRequest.Requirement.CreationDate.ToString(),
                        StrModifiedDate = s.ChangeRequest.Requirement.ModifiedDate.ToString()


                    }).ToList();
            return Json(hasil, JsonRequestBehavior.AllowGet);
        }
        //get Data Change reuestname Document
        public JsonResult GetDataChangeRequirmentDocument(int? id, int? page, string searchDR = null)
        {
            if (page == null)
            {
                page = 1;
            }
            int x = Convert.ToInt32(id);
            int pageSize = 5;
            int length = db.ChangeRequestDocuments.Where(d => d.ChangeRequest.Requirement.ModulId== id).Count();
            int pageNumber = (page ?? 1);
            var dataCRD = db.ChangeRequestDocuments.Where(d => d.ChangeRequest.Requirement.ModulId == id).AsQueryable();
            if (searchDR != "")
            {
                int lvl = 0;
                try
                {
                    lvl = Convert.ToInt32(searchDR);
                }
                catch(Exception e)
                {

                }

                DateTime y = new DateTime(9999, 1, 1);
                try
                {
                    y = Convert.ToDateTime(searchDR);
                }
                catch (Exception e) { }

                DateTime xx = new DateTime(9999, 1, 1);
                try
                {
                    xx = Convert.ToDateTime(searchDR);
                }
                catch (Exception e) { }



                dataCRD = dataCRD.Where(p => p.ChangeRequest.Requirement.RequirementNumber.ToLower().Contains(searchDR.ToLower())
                            || p.ChangeRequest.Description.ToLower().Contains(searchDR.ToLower()) || p.ChangeRequest.Requirement.Project.ProjectName.ToLower().Contains(searchDR.ToLower())
                            || p.ChangeRequest.Requirement.Modul.ModulName.ToLower().Contains(searchDR.ToLower()) || p.ChangeRequest.Requirement.Description.ToLower().Contains(searchDR.ToLower())
                            || p.ChangeRequest.Requirement.Level==lvl || p.ChangeRequest.Requirement.ModifiedBy.ToLower().Contains(searchDR.ToLower())
                            || p.ChangeRequest.Requirement.CreationBy.ToLower().Contains(searchDR.ToLower()) || p.ChangeRequest.Requirement.CreationDate == y
                            || p.ChangeRequest.Requirement.ModifiedDate==xx
                ).AsQueryable();
                length = dataCRD.Count();
            }
            
            var hasil = dataCRD.AsEnumerable().
                    OrderBy(d => d.DocumentName)
                    .Select(s => new ChangeRequestDocumentViewModel()
                    {
                        Version=s.Version,
                        RequirementNumber=s.ChangeRequest.Requirement.RequirementNumber,
                        DocumentName = s.DocumentName,
                        RequirementDescription=s.ChangeRequest.Requirement.Description,
                        ProjectName=s.ChangeRequest.Requirement.Project.ProjectName,
                        ModulName=s.ChangeRequest.Requirement.Modul.ModulName,
                        Description=s.ChangeRequest.Description,
                        Level=s.ChangeRequest.Requirement.Level,
                        ChangeRequestId=s.ChangeRequestId,
                        CreationBy=s.ChangeRequest.Requirement.CreationBy,
                        CreationDate=s.ChangeRequest.Requirement.CreationDate,
                        DocumentId=s.DocumentId,
                        DocumentTypeId=s.DocumentTypeId,
                        FilePath=s.FilePath,
                        ModifiedBy=s.ChangeRequest.Requirement.ModifiedBy,
                        ModifiedDate=s.ChangeRequest.Requirement.ModifiedDate,
                        ModulNumber=s.ChangeRequest.Requirement.Modul.ModulNumber,
                        lenght=length,
                        StrCreationDate=s.ChangeRequest.Requirement.CreationDate.ToString(),
                        StrModifiedDate=s.ChangeRequest.Requirement.ModifiedDate.ToString()


                    }).ToPagedList(pageNumber, pageSize);
            return Json(hasil, JsonRequestBehavior.AllowGet);
        }

     
        // get Data Requirment Document
        public JsonResult GetDataRequirmentDocument(int? id, int? page, string searchDR = null)
        {
            if (page == null)
            {
                page = 1;
            }
            int x = Convert.ToInt32(id);
            int pageSize = 5;
            int length = db.RequirementDocuments.Where(d => d.Requirement.ModulId == x).Count();
            int pageNumber = (page ?? 1);
            var dataRD = db.RequirementDocuments.Where(d => d.Requirement.ModulId == x).AsQueryable();

            if (searchDR != "")
            {
                dataRD = dataRD.Where(s => s.DocumentName.ToLower().Contains(searchDR.ToLower()) || s.Requirement.RequirementNumber.ToLower().Contains(searchDR.ToLower())
                        || s.DocumentType.Description.ToLower().Contains(searchDR.ToLower()) || s.FilePath.ToLower().Contains(searchDR.ToLower()) || s.Version.ToLower().Contains(searchDR.ToLower())
                );
                length = dataRD.Count();
            }
            var hasil = dataRD
                        .OrderBy(o => o.DocumentName)
                        .Select(s => new RequirmentDocumentViewModel()
                        {
                            DocumentName = s.DocumentName,
                            Length = length,
                            FilePath = s.FilePath,
                            DocumentId = s.DocumentId,
                            DocumentTypeId = s.Requirement.ModulId,
                            RequimentId = s.RequimentId,
                            requirmentNumber = s.Requirement.RequirementNumber,
                            Version = s.Version,
                            documentType=s.DocumentType.Description
                        }).ToPagedList(pageNumber, pageSize);
            return Json(hasil, JsonRequestBehavior.AllowGet);
            
        }
        public JsonResult GetDataRequirmentDocumentById(string id)
        {
            int x = Convert.ToInt32(id);
            var data = db.RequirementDocuments.Where(p => p.DocumentId == x)
                .Select(s => new RequirmentDocumentViewModel()
                {
                    DocumentName = s.DocumentName,

                    FilePath = s.FilePath,
                    DocumentId = s.DocumentId,
                    DocumentTypeId = s.Requirement.ModulId,
                    RequimentId = s.RequimentId,
                    requirmentNumber = s.Requirement.RequirementNumber,
                    Version = s.Version,
                    documentType = s.DocumentType.Description
                }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteModulDependency(int? id)
        {
            var dataMD = db.ModulDependencies.Find(id);
            db.ModulDependencies.Remove(dataMD);
            db.SaveChanges();
            return View();
        }
        // save data requirment document
        public ActionResult EditReqDoc(
         string DocumentId,
         string RequimentId,
         string DocumentTypeId,
         string DocumentName,
         string FilePath,
         string Version)
        {
            var result = new jsonMessage();
            try
            {
                RequirementDocument reDoc = db.RequirementDocuments.Find(Convert.ToInt32(DocumentId));
                reDoc.RequimentId = Convert.ToInt32(RequimentId);
                if(DocumentName!="")
                reDoc.DocumentName = DocumentName;
                if(FilePath!="")
                reDoc.FilePath = FilePath;
                if(Version!="")
                reDoc.Version = Version;
                if (DocumentTypeId != "")
                {
                    reDoc.DocumentTypeId = Convert.ToInt32(DocumentTypeId);
                }
                result.Message = "Ubah Berhasil";
                result.Status = true;
                db.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                result.Message = e + "Permintaan Anda tidak bisa diproses. Coba lagi nanti.";
                result.Status = false;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
            //return Redirect("EditRe");
        }

        //get Data Modul Depedency
        public JsonResult GetDataModulDependency(int? id,int? page,string searchMD= null)
        {
            if (page == null)
            {
                page = 1;
            }
            int pageSize = 3;
            int length = db.ModulDependencies.Where(d => d.ModulId == id).ToList().Count();
            int pageNumber = (page ?? 1);
            if (searchMD != "") {
                length = db.ModulDependencies.Where(d => d.ModulId == id 
                && d.Modul.ModulName.ToLower().Contains(searchMD.ToLower())).ToList().Count();
            }
            var dataMD = db.ModulDependencies.Where(m => m.ModulId == id).AsQueryable();
            if (searchMD != "")
            {
                dataMD = dataMD.Where(d => d.Modul.ModulName.ToLower().Contains(searchMD.ToLower())
                        || d.Modul.ModulNumber.ToLower().Contains(searchMD.ToLower())
                ).AsQueryable();
            }
                var hasil=dataMD.AsEnumerable().OrderBy(o=>o.DependencyId)
                .Select(s=>new ModulDependencyViewModul() {
                    DependencyId=s.DependencyId,
                    ModulId=s.ModulId,
                    DependToModulId=s.DependToModulId,
                    Length=length
                }).ToPagedList(pageNumber, pageSize);
            int idx = 0;
            foreach(var x in hasil)
            {
                var tampModul = db.Moduls.Find(x.DependToModulId);
                hasil[idx].ModulName = tampModul.ModulName;
                hasil[idx].ModulNumber = tampModul.ModulNumber;
                idx += 1;
            }
           
            return Json(hasil, JsonRequestBehavior.AllowGet);
        }
        public JsonResult PopUpModulDependency(int? id,int? idDependency)
        {

            ViewBag.ModulId = id;
            ViewBag.ModulNumber = db.Moduls.Find(id).ModulNumber;
            ViewBag.ModulName = db.Moduls.Find(id).ModulName;
            ViewBag.DependencyId = null;

            if (idDependency != null)
            {
                ViewBag.DependencyId = idDependency;
            }
            ViewBag.ListModul = new SelectList(db.Moduls, "ModulId", "ModulName");
            var dataEdit = db.ModulDependencies.Where(a => a.DependencyId == idDependency).Select(s => new ModulDependencyViewModul()
            {
                DependencyId = s.DependencyId,
                ModulId = s.ModulId,
                DependToModulId = s.DependToModulId,

            }).ToList();
            int idx = 0;
            foreach (var x in dataEdit)
            {
                var tampModul = db.Moduls.Find(x.DependToModulId);
                dataEdit[idx].ModulName = tampModul.ModulName;
                dataEdit[idx].ModulNumber = tampModul.ModulNumber;
                idx += 1;
            }
            return Json(dataEdit, JsonRequestBehavior.AllowGet);
        }
        //get data for edit project time line 
        public JsonResult GetDataProjectTimeLineForEdit(int? id) {
            var dataPRT = db.ProjectTimelines.Where(d => d.TimelineId == id).AsQueryable();
            var hasil = dataPRT.AsEnumerable()
                           .OrderBy(p => p.ProjectId)
                           .Select(s => new ProjectTimeLineViewModel()
                           {
                               ModulId = s.ModulId,
                               EndDate = s.EndDate,
                               ProjectId = s.ProjectId,
                               ProjectScheduleId = s.ProjectScheduleId,
                               StartDate = s.StartDate,
                               TimelineId = s.TimelineId,
                               EndDateStr = s.EndDate.ToString(),
                               StartDateStr = s.StartDate.ToString(),
                               Version=s.ProjectSchedule.Version

                           }).ToList();
            int idx = 0;
            foreach (var x in hasil)
            {
                var tampModul = db.Moduls.Find(x.ModulId);
                var tampProject = db.Projects.Find(x.ProjectId);
                hasil[idx].ProjectName = tampProject.ProjectName;
                hasil[idx].ModulName = tampModul.ModulName;
                idx += 1;
            }
            return Json(hasil, JsonRequestBehavior.AllowGet);
        }
        //save Modul Document
        public JsonResult SaveModDoc(
       string ModulID,
       string DocumentTypeid,
       string name,
       string file,
       string version)
        {
            var result = new jsonMessage();
            var Prodoc =new ModulDocument();
            Prodoc.DocumentTypeId = Convert.ToInt32(DocumentTypeid);
            Prodoc.Version = version;
            Prodoc.ModulId = Convert.ToInt32(ModulID);
            Prodoc.DocumentName = name;
            Prodoc.FilePath = file;
            db.ModulDocuments.Add(Prodoc);
            db.SaveChanges();
            result.Message = "Add Data Succes";
            return Json(result, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetDataDocument(int? id, int? page, string searchDC = null)
        {
            if (page == null)
            {
                page = 1;
            }

            int pageSize = 5;
            int length = db.ModulDocuments.Where(d => d.ModulId == id).ToList().Count();
            int pageNumber = (page ?? 1);

            var dataDoc = db.ModulDocuments.Where(d => d.ModulId == id).ToList();
            if (searchDC != "")
            {
                dataDoc = dataDoc.Where(p => p.DocumentName.ToLower().
                  Contains(searchDC.ToLower()) || p.DocumentType.Description.ToLower().Contains(searchDC.ToLower())
                  || p.FilePath.ToLower().Contains(searchDC.ToLower()) || p.Version.ToLower().Contains(searchDC.ToLower())).ToList();
                length = dataDoc.Count();
            }
            var hasil = dataDoc.OrderBy(p => p.DocumentName)
               .Select(s => new ModulDocumentViewModel()
               {
                   DocumentId = s.DocumentId,
                   ModulId = s.ModulId,
                   DocumentName = s.DocumentName,
                   DocumentTypeId = s.DocumentTypeId,
                   FilePath = s.FilePath,
                   Version = s.Version,
                   length = length
               }).ToPagedList(pageNumber, pageSize);
            int idx = 0;
            foreach (var x in hasil)
            {
                var doc = db.DocumentTypes.Find(x.DocumentTypeId);
                hasil[idx].DocumentType = doc.Description;
                idx += 1;

            }
            return Json(hasil, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDataByIdModulDocument(int? ModulDocumentId)
        {
            var dataMDocument = db.ModulDocuments.Where(p => p.DocumentId == ModulDocumentId)
                  .Select(s => new ModulDocumentViewModel()
                  {
                      DocumentId = s.DocumentId,
                      ModulId = s.ModulId,
                      DocumentName = s.DocumentName,
                      DocumentTypeId = s.DocumentTypeId,
                      FilePath = s.FilePath,
                      Version = s.Version,
                  }).ToList();
            return Json(dataMDocument, JsonRequestBehavior.AllowGet);
        }
        //get data project time line
        public JsonResult GetDataProjectTimeLine(int? id,int? page,string searchPRT=null)
        
        {
            if (page == null)
            {
                page = 1;
            }

            int pageSize = 5;
            int length = db.ProjectTimelines.Where(d => d.ModulId == id).ToList().Count();
            int pageNumber = (page ?? 1);
           
            var dataPRT = db.ProjectTimelines.Where(d => d.ModulId == id).AsQueryable();
            if (searchPRT != "")
            {
                DateTime y = new DateTime(9999, 1, 1);
                try
                {
                    y = Convert.ToDateTime(searchPRT);
                }
                catch (Exception e) { }
                DateTime x = new DateTime(9999, 1, 1);
                try
                {
                    x = Convert.ToDateTime(searchPRT);
                }
                catch (Exception e) { }
                dataPRT = dataPRT.Where(p => p.Project.ProjectName.ToLower().Contains(searchPRT.ToLower()) || p.Modul.ModulName.ToLower().Contains(searchPRT.ToLower())
                            || p.ProjectSchedule.Version.ToLower().Contains(searchPRT.ToLower()) || p.StartDate==y || p.EndDate==x
                ).AsQueryable();
                length = dataPRT.Count();
            }
            var hasil=dataPRT.AsEnumerable()
                            .OrderBy(p=>p.ProjectId)
                            .Select(s => new ProjectTimeLineViewModel()
                            {
                                ModulId = s.ModulId,
                                EndDate = s.EndDate,
                                ProjectId = s.ProjectId,
                                ProjectScheduleId = s.ProjectScheduleId,
                                StartDate = s.StartDate,
                                TimelineId = s.TimelineId,
                                EndDateStr=s.EndDate.ToString(),
                                StartDateStr=s.StartDate.ToString(),
                                Length=length
                        
                            }).ToPagedList(pageNumber, pageSize);
            int idx = 0;
            foreach (var x in hasil)
            {
                var tampModul = db.Moduls.Find(x.ModulId);
                var tampProject = db.Projects.Find(x.ProjectId);
                var tampV = db.ProjectSchedules.Find(x.ProjectScheduleId);
                hasil[idx].ProjectName = tampProject.ProjectName;
                hasil[idx].ModulName = tampModul.ModulName;
                hasil[idx].Version = tampV.Version;
                idx += 1;
            }
            return Json(hasil, JsonRequestBehavior.AllowGet);

        }
        // get data requirment
        public JsonResult GetDataRequirment(int? id, int? page, string searchR = null)
        {
            if (page == null)
            {
                page = 1;
            }

            int pageSize = 5;
            int length = db.Requirements.Where(d => d.ModulId == id).ToList().Count();
            int pageNumber = (page ?? 1);
          
            var dataDoc = db.Requirements.Where(d => d.ModulId == id).AsQueryable();
            if (searchR != "")
            {
                int lvl = 0;
                try
                {
                    lvl = Convert.ToInt32(lvl);
                }
                catch (Exception e)
                {

                }
                DateTime y = new DateTime(9999, 1, 1);
                try
                {
                    y = Convert.ToDateTime(searchR);
                }
                catch (Exception e) { }
                DateTime x = new DateTime(9999, 1, 1);
                try
                {
                    x = Convert.ToDateTime(searchR);
                }
                catch (Exception e) { }
                dataDoc = dataDoc.Where(p => p.RequirementNumber.ToLower().Contains(searchR.ToLower()) || p.RequimentType.Description.ToLower().Contains(searchR.ToLower())
                            || p.Description.ToLower().Contains(searchR.ToLower()) || p.Level==lvl || p.CreationDate==y || p.ModifiedDate==x 
                ).AsQueryable();
                length = dataDoc.Count();
            }
            var hasil=dataDoc.AsEnumerable().OrderBy(o=>o.Description)
       .Select(s => new RequirmentViewModel()
       {
           CreationBy = s.CreationBy,
           RequimentId = s.RequimentId,
           CreationDate = s.CreationDate,
           Description = s.Description,
           Details = s.Details,
           Level = s.Level,
           ModifiedBy = s.ModifiedBy,
           ModifiedDate = s.ModifiedDate,
           ModulId = s.ModulId,
           ProjectId = s.ProjectId,
           RequimentTypeId = s.RequimentTypeId,
           RequirementNumber = s.RequirementNumber,
           CreationDateStr = s.CreationDate.ToString(),
           ModifiedDateStr = s.ModifiedDate.ToString(),
           length = length
       }).ToPagedList(pageNumber, pageSize);
            int dx = 0;
            foreach (var x in hasil)
            {
                var rType = db.RequimentTypes.Find(x.RequimentTypeId);
                hasil[dx].RequiermentType = rType.Description;
                dx += 1;
            }
            return Json(hasil, JsonRequestBehavior.AllowGet);
        }

     
        //Edit Modul
        public JsonResult EditDataModul(int? id,int? projectName,string modulNUmber,string modulName,string parentModul,string pic,string modulStatus)
        {
            var dataModul = db.Moduls.Find(id);
            dataModul.ProjectId = projectName;
            dataModul.ModulName = modulName;
            dataModul.ModulNumber = modulNUmber;
            return Json(dataModul, JsonRequestBehavior.AllowGet);

        }
        //cek Modul Number
        public JsonResult CekModelNumber(string id)
        {
         
            var datanumber = db.Moduls.Where(m => m.ModulId == 1 && m.ModulNumber.Equals(id))
                .Select(s=>new ModulViewModel() {
                    ModulNumber=s.ModulNumber
                }).ToList();
            return Json(datanumber, JsonRequestBehavior.AllowGet);

        }
        // get data change request
        public JsonResult GetDataChangeRequest(int? id,int? page,string searchText=null)
        {
            if (page == null)
            {
                page = 1;
            }

            int pageSize = 5;
            int length = db.ChangeRequests.Where(s=>s.Requirement.ModulId==id).ToList().Count();
            int pageNumber = (page ?? 1);
            //int pageSize = 10;
          
            var dataCR = db.ChangeRequests.Where(s=> s.Requirement.ModulId == 1).ToList();
            if (searchText != "")
            {
                int mandays = 0;
                try
                {
                    mandays = Convert.ToInt32(searchText);
                }catch(Exception e) { }
                DateTime y = new DateTime(9999, 1, 1);
                try
                {
                    y = Convert.ToDateTime(searchText);
                }
                catch (Exception e) { }
                dataCR = dataCR.Where(s => s.ChangeRequestNumber.ToLower().Contains(searchText.ToLower()) || s.Requirement.Description.ToLower().Contains(searchText.ToLower())
                        || s.Requirement.RequirementNumber.ToLower().Contains(searchText.ToLower()) || s.Description.ToLower().Contains(searchText.ToLower())
                        || s.MandaysEstimation==mandays || s.ChangeRequestDate==y

                ).ToList();
                length = dataCR.Count();
            }

            var hasil=dataCR
                .OrderBy(o=>o.Description)
                .Select(s => new ChangeRequesViewModel()
                {
                    ChangeRequestDate = s.ChangeRequestDate,
                    ChangeRequestId = s.ChangeRequestId,
                    ChangeRequestNumber = s.ChangeRequestNumber,
                    Description = s.Description,
                    MandaysEstimation = s.MandaysEstimation,
                    RequimentId = s.RequimentId,
                    lenght=length,
                    RequirementDescription=s.Requirement.Description,
                    RequirementRequestNumber=s.Requirement.RequirementNumber,
                    StrChangeRequestDate=s.ChangeRequestDate.ToString()
                }).ToPagedList(pageNumber, pageSize);

            //here i am returning multiple data to my jquerry  user and CountUser  
            return Json(hasil, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DataParent(int? id)
        {
            var dataModul = db.Moduls.Where(m => m.ModulId == id).Select(s => new ModulViewModel()
            {
                ModulId = s.ModulId,
                Description = s.Description,
                MadaysEstimation = s.MadaysEstimation,
                ModulStatusId = s.ModulStatusId,
                ModulName = s.ModulName,
                ModulNumber = s.ModulNumber,
                ParentModulId = s.ParentModulId,
                PIC = s.PIC,
                ProjectId = s.ProjectId

            }).ToList();

            var dataPar = db.Moduls.Where(m => m.ProjectId == dataModul[0].ProjectId).Select(s => new ModulViewModel()
            {
                ModulId = s.ModulId,
                Description = s.Description,
                MadaysEstimation = s.MadaysEstimation,
                ModulStatusId = s.ModulStatusId,
                ModulName = s.ModulName,
                ModulNumber = s.ModulNumber,
                ParentModulId = s.ParentModulId,
                PIC = s.PIC,
                ProjectId = s.ProjectId

            }).ToList();
            return Json(dataPar, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult UpdateData(int? id,string projectName,string modulStatusIdAwal,string modulNumber,string modulName,string startDateActual,string parentModulId,
            string endDateActual,string PIC,string mandaysestimation)
        {
            var dataModul = db.Moduls.Find(id);
            var dataProject = db.Projects.Where(p => p.ProjectName.ToLower().Contains(projectName.ToLower())).FirstOrDefault(); ;
            dataModul.ProjectId = dataProject.ProjectId;
            int idMS = Convert.ToInt32(modulStatusIdAwal);
            var modulStatus = db.ModulStatus.Find(idMS);
            dataModul.ModulStatusId = idMS;
            dataModul.ModulName = modulName;
            dataModul.ModulNumber = modulNumber;
            dataModul.PIC = PIC;
            decimal x = Convert.ToDecimal(mandaysestimation);
            dataModul.MadaysEstimation = x;
            if (endDateActual != "")
            {
                DateTime tamp = Convert.ToDateTime(endDateActual);
                dataProject.EndDateActual = tamp;
            }
            else
            {
                dataProject.EndDateActual = dataProject.EndDateActual;
            }
            if (startDateActual != "")
            {
                DateTime tamp = Convert.ToDateTime(startDateActual);
                dataProject.StartDateActual = tamp;
            }
            else
            {
                dataProject.StartDateActual = dataProject.StartDateActual;
            }
            db.SaveChanges();
            var hasil = db.Moduls.Where(p => p.ModulId == id).Select(s => new ModulViewModel()
            {
                ModulId = s.ModulId,
                ModulName = s.ModulName,
                ProjectId=s.ProjectId
            }).ToList().FirstOrDefault();
            return Json(hasil, JsonRequestBehavior.AllowGet);
        }
        public JsonResult EditViewJs(int? id,int? page)
        {
            var dataModul = db.Moduls.Where(m => m.ModulId == id).Select(s => new ModulViewModel()
            {
                ModulId = s.ModulId,
                Description = s.Description,
                MadaysEstimation = s.MadaysEstimation,
                ModulStatusId = s.ModulStatusId,
                ModulName = s.ModulName,
                ModulNumber = s.ModulNumber,
                ParentModulId = s.ParentModulId,
                PIC = s.PIC,
                ProjectId = s.ProjectId,
                EndDateActual=s.Project.EndDateActual.ToString(),
                StartDateActual=s.Project.StartDateActual.ToString(),
                ProjectName=s.Project.ProjectName
            }).ToList().FirstOrDefault();
            var dataPM = db.Moduls.Where(p => p.ModulId == dataModul.ParentModulId).FirstOrDefault();
            dataModul.ParentModul = dataPM.ModulName;
            var dataPrject = db.Projects.Where(p => p.ProjectId == dataModul.ProjectId).FirstOrDefault();
          
            return Json(dataModul, JsonRequestBehavior.AllowGet);
        }
    }
}
