﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjectManagement.Entities;
using ProjectManagement.Models;
using PagedList;

namespace ProjectManagement.Controllers
{
    public class ProjectScheduleController : Controller
    {
        private ProjectManagementDBContext db = new ProjectManagementDBContext();

        // GET: ProjectSchedule
        public ActionResult Index()
        {
            var projectSchedules = db.ProjectSchedules.Include(p => p.Project);
            return View(projectSchedules.ToList());
        }

        public ActionResult Edit(int? id)
        {
            var PS = db.ProjectSchedules.Find(id);

            ViewBag.ProjectId = PS.ProjectId;
            ViewBag.ModulId = 3;

            ViewBag.ProjectName = db.Projects.Find(PS.ProjectId).ProjectName;
            ViewBag.ModulName = db.Moduls.Find(3).ModulName;
            ViewBag.Version = PS.Version;

            ViewBag.ProjectScheduleId = id;

            return View();
        }

        public ActionResult Edit2(int? id)
        {
            var PS = db.ProjectSchedules.Find(id);

            ViewBag.ProjectId = PS.ProjectId;
            ViewBag.ModulId = 3;

            ViewBag.ProjectName = db.Projects.Find(PS.ProjectId).ProjectName;
            ViewBag.ModulName = db.Moduls.Find(1).ModulName;
            ViewBag.Version = PS.Version;

            ViewBag.ProjectScheduleId = id;

            return View();
        }

        public JsonResult LoadData(int? ProjectScheduleId)
        {
            var projectSchedule = db.ProjectSchedules.Find(ProjectScheduleId);
            ProjectScheduleViewModel proSchVm = new ProjectScheduleViewModel()
            {
                ProjectScheduleId = projectSchedule.ProjectScheduleId,
                ProjectId = projectSchedule.ProjectId,
                ProjectName = projectSchedule.Project.ProjectName,
                Version = projectSchedule.Version,
                StartDate = projectSchedule.StartDate.ToString(),
                EndDate = projectSchedule.EndDate.ToString()
            };

            return Json(proSchVm, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveData(ProjectScheduleViewModel valueEdited)
        {
            var projectScheduleEdited = db.ProjectSchedules.Find(valueEdited.ProjectScheduleId);
            projectScheduleEdited.ProjectId = valueEdited.ProjectId;
            projectScheduleEdited.Version = valueEdited.Version;
            var startDate = Convert.ToDateTime(valueEdited.StartDate);
            var endDate = Convert.ToDateTime(valueEdited.EndDate);
            projectScheduleEdited.StartDate = startDate;
            projectScheduleEdited.EndDate = endDate;
            db.SaveChanges();
            return Json("Succes Edited for ProjectScheduleId " + valueEdited.ProjectScheduleId, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ListDataProjectTimeline(int ProjectScheduleId, int? page)
        {
            if (page == null)
            {
                page = 1;
            }
            int pageSize = 5;
            int length = db.ProjectTimelines.Where(z => z.ProjectScheduleId == ProjectScheduleId).ToList().Count();
            int pageNumber = (page ?? 1);
            var proTime = db.ProjectTimelines.Where(z => z.ProjectScheduleId == ProjectScheduleId).OrderBy(d => d.TimelineId).
                Select(s => new ProjectTimeLineViewModel()
                {
                    TimelineId = s.TimelineId,
                    ProjectId = s.ProjectId,
                    ProjectScheduleId = s.ProjectScheduleId,
                    ModulId = s.ModulId,
                    ProjectName = s.Project.ProjectName,
                    ModulNumber = s.Modul.ModulNumber,
                    ModulName = s.Modul.ModulName,
                    Version = s.ProjectSchedule.Version,
                    StartDateStr = s.StartDate.ToString(),
                    EndDateStr = s.EndDate.ToString()
                }).ToPagedList(pageNumber, pageSize);
            return Json(proTime, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDataCRDById(int? id)
        {
            var data = db.ProjectTimelines.Find(id);


            string Y, M, D;
            Y = "10";
            M = "03";
            D = "2018";

            ProjectTimeLineViewModel projectSche = new ProjectTimeLineViewModel()
            {
                TimelineId = data.TimelineId,
                ProjectScheduleId = data.ProjectScheduleId,
                ProjectId = data.ProjectId,
                ModulId = data.ModulId,
                StartDate = data.StartDate,
                EndDate = data.EndDate,
                SD = Y + "-" + M + "-" + D
                //SD = "10-10-2010"
            };

            return Json(projectSche, JsonRequestBehavior.AllowGet);
        }        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
