﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;
using ProjectManagement.Entities;
using ProjectManagement.Models;

namespace ProjectManagement.Controllers
{
    /*
     
    Author             : Wawan Setiawan
    Created date       : Rabu, 21/11/2018
    Date Modified      : Jum'at, 23/11/2018
    Modified By        : Nispi A A
    Date modified      : 30-11-2018
    Additional         : Menambahkan Fungsi Delete
      Modified By        : Nispi A A
    Date modified      : 3-12-2018/5-12-2018
    Additional         : Menambahkan Fungsi 
         */
    public class ProjectController : Controller
    {
        private ProjectManagementDBContext db = new ProjectManagementDBContext();

        // GET: Projects
        public ActionResult Index()
        {

            ViewBag.ClientId = new SelectList(db.Clients, "ClientId", "Name");
            ViewBag.ProjectStatusId = new SelectList(db.ProjectStatus, "ProjectStatusId", "Description");
            return View();
        }
        public ActionResult Edit(int id)
        {
            ViewBag.ProjectId = id;
            //var pro = db.Projects.Where(c => c.ProjectId == id).Select(d => new ProjectViewModel()
            //{
            //    ProjectName=d.ProjectName
            //}).ToList();
            //ViewBag.ProjectName = db.Projects.Find(id).ProjectName;
            var pro = db.Projects.Find(id);
            //ViewBag.StartDateAct = pro.StartDateActual;
            ViewBag.ListModul = new SelectList(db.Moduls, "ModulId", "ModulName");
            ViewBag.DocTypeId = new SelectList(db.DocumentTypes, "DocumentTypeId", "Description");
            ViewBag.ModulStatus = new SelectList(db.ModulStatus, "ModulStatusId", "Description");
            ViewBag.ModulId = new SelectList(db.Moduls, "ModulId", "ModulName");
            ViewBag.ClientId = new SelectList(db.Clients, "ClientId", "Name");
            ViewBag.ProjectStatusId = new SelectList(db.ProjectStatus, "ProjectStatusId", "Description");
            return View();
        }

        public ActionResult Delete(int id)
        {
            Project pro = db.Projects.Find(id);
            db.Projects.Remove(pro);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public JsonResult ListDataModDep(int? ProjectId, int? page, string r = null)
        {

            if (page == null)
            {
                page = 1;
            }

            int pageSize = 5;
            int length = db.ModulDependencies.Where(z => z.Modul.ProjectId == ProjectId).ToList().Count();

            int pageNumber = (page ?? 1);
            var MOddep = db.ModulDependencies.AsQueryable();
            if (r != "")
            {
                MOddep = MOddep.Where(z => z.Modul.ModulName.ToLower().Contains(r.ToLower()) || z.Modul.ModulNumber.Contains(r)).AsQueryable();
                length = MOddep.Where(z => z.Modul.ProjectId == ProjectId).ToList().Count();
            }
            var ModDep = MOddep.Where(s => s.Modul.ProjectId == ProjectId)
                .OrderBy(x => x.DependencyId)
                .Select(z => new ModulDependencyViewModul()
                {
                    DependencyId = z.DependencyId,
                    DependToModulId = z.DependToModulId,
                    ModulName = z.Modul.ModulName,
                    ModulNumber = z.Modul.ModulNumber,
                    Length = length
                }).ToPagedList(pageNumber, pageSize);
            return Json(ModDep, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ListDataReqDoc(int? ProjectId, int? page, string r)

        {
            if (page == null)
            {
                page = 1;
            }
            int pageSize = 5;
            int length = db.RequirementDocuments.Where(z => z.Requirement.ProjectId == ProjectId).ToList().Count();

            int pageNumber = (page ?? 1);
            var Req = db.RequirementDocuments.AsQueryable();
            if (r != "")
            {
                Req = Req.Where(z => z.DocumentName.ToLower().Contains(r.ToLower())||z.Requirement.RequirementNumber.Contains(r)
                || z.DocumentType.Description.ToLower().Contains(r.ToLower())||z.FilePath.ToLower().Contains(r.ToLower())
                || z.Version.Contains(r)
                ).AsQueryable();
                length = Req.Where(z => z.Requirement.ProjectId == ProjectId).ToList().Count();
            }
            var reqdoc = Req.Where(s => s.Requirement.ProjectId == ProjectId)
                .OrderBy(x => x.RequimentId)
                .Select(z => new RequirmentDocumentViewModel()
                {
                    DocumentId = z.DocumentId,
                    requirmentNumber = z.Requirement.RequirementNumber,
                    documentType = z.DocumentType.Description,
                    DocumentName = z.DocumentName,
                    FilePath = z.FilePath,
                    Version = z.Version,
                    Length = length
                }).ToPagedList(pageNumber, pageSize);
            return Json(reqdoc, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ListDataModDoc(int? ProjectId, int? page, string r)
        {

            if (page == null)
            {
                page = 1;
            }

            int pageSize = 5;
            int length = db.ModulDocuments.Where(z => z.Modul.ProjectId == ProjectId).ToList().Count();
            int pageNumber = (page ?? 1);
            var ModDoc = db.ModulDocuments.AsQueryable();
            if (r != "")
            {
                ModDoc = ModDoc.Where(z => z.DocumentName.ToLower().Contains(r.ToLower()) || 
                z.DocumentType.Description.ToLower().Contains(r.ToLower()) || z.FilePath.ToLower().Contains(r.ToLower())
                || z.Version.Contains(r)
                ).AsQueryable();
                length = ModDoc.Where(z => z.Modul.ProjectId == ProjectId).ToList().Count();
            }
            var Moddoc = ModDoc.Where(s => s.Modul.ProjectId == ProjectId)
                .OrderBy(x => x.DocumentId)
                .Select(z => new ModulDocumentViewModel()
                {
                    DocumentId = z.DocumentId,
                    DocumentType = z.DocumentType.Description,
                    DocumentName = z.DocumentName,
                    FilePath = z.FilePath,
                    Version = z.Version,
                    length = length
                }).ToPagedList(pageNumber, pageSize);
            return Json(Moddoc, JsonRequestBehavior.AllowGet);

        }
        public JsonResult ListDataProDoc(int? ProjectId, int? page, string r)
        {
            if (page == null)
            {
                page = 1;
            }
            int pageSize = 5;
            int length = db.ProjectDocuments.Where(z => z.ProjectId == ProjectId).ToList().Count();
            int pageNumber = (page ?? 1);
            var ProDoc = db.ProjectDocuments.AsQueryable();
            if (r != "")
            {
                ProDoc = ProDoc.Where(z => z.DocumentName.ToLower().Contains(r.ToLower())
                || z.DocumentType.Description.ToLower().Contains(r.ToLower()) || z.FilePath.ToLower().Contains(r.ToLower())
                || z.Version.Contains(r)
                ).AsQueryable();
                length = ProDoc.Where(z => z.ProjectId == ProjectId).ToList().Count();

            }
            var proDoc = ProDoc.Where(d => d.ProjectId == ProjectId)
                .OrderBy(x => x.ProjectId)
                .Select(wer => new ProjectDocumentViewModel()
                {
                    DocumentId = wer.DocumentId,
                    DocumentType = wer.DocumentType.Description,
                    DocumentName = wer.DocumentName,
                    FilePath = wer.FilePath,
                    Version = wer.Version,
                    Length = length
                }).ToPagedList(pageNumber, pageSize);
            return Json(proDoc, JsonRequestBehavior.AllowGet);

        }

        public JsonResult ListDataChangRequest(int? ProjectId, int? page, string r)
        {
            if (page == null)
            {
                page = 1;
            }

            int pageSize = 5;
            int length = db.ChangeRequests.Where(z => z.Requirement.ProjectId == ProjectId).ToList().Count();
            int pageNumber = (page ?? 1);
            var Req = db.ChangeRequests.AsQueryable();
            if (r != "")
            {
                Req = Req.Where(z => z.Requirement.Modul.ModulName.ToLower().Contains(r.ToLower())
                || z.Requirement.Modul.ModulNumber.Contains(r) || z.Requirement.RequirementNumber.Contains(r)
                || z.Requirement.Description.ToLower().Contains(r.ToLower()) || z.MandaysEstimation.ToString().Contains(r)
                ).AsQueryable();
                length = Req.Where(z => z.Requirement.ProjectId == ProjectId).ToList().Count();
            }

            var change = Req.Where(z => z.Requirement.ProjectId == ProjectId)
                .OrderBy(x => x.ChangeRequestId)
                .Select(d => new ChangeRequesViewModel()
                {
                    ChangeRequestId = d.ChangeRequestId,
                    ModulName = d.Requirement.Modul.ModulName,
                    modulNumber = d.Requirement.Modul.ModulNumber,
                    reqnumber = d.Requirement.RequirementNumber,
                    Description = d.Requirement.Description,
                    StrChangeRequestDate = d.ChangeRequestDate.ToString(),
                    MandaysEstimation = d.MandaysEstimation,
                    lenght = length

                }).ToPagedList(pageNumber, pageSize);
            return Json(change, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ListDataProjectSchedule(int? ProjectId, int? page, string r)
        {
            if (page == null)
            {
                page = 1;
            }
            int pageSize = 5;
            int length = db.ProjectSchedules.Where(z => z.ProjectId == ProjectId).ToList().Count();
            int pageNumber = (page ?? 1);

            var Re = db.ProjectSchedules.AsQueryable();
            if (r != "")
            {
                Re = Re.Where(z => z.Project.ProjectName.ToLower().Contains(r.ToLower())||z.Version.Contains(r)
                || z.StartDate.ToString().Contains(r)).AsQueryable();
                length = Re.Where(z => z.ProjectId == ProjectId).ToList().Count();
            }
            var Pro = Re.Where(s => s.ProjectId == ProjectId)
                .OrderBy(x => x.ProjectId)
                .Select(z => new ProjectScheduleViewModel()
                {
                    ProjectScheduleId = z.ProjectScheduleId,
                    ProjectName = z.Project.ProjectName,
                    Version = z.Version,
                    StartDate = z.StartDate.ToString(),
                    EndDate = z.EndDate.ToString(),
                    Lenght = length
                }).ToPagedList(pageNumber, pageSize);
            return Json(Pro, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ListDataReq(int? ProjectId, int? page, string rs)
        {
            if (page == null)
            {
                page = 1;
            }
            int pageSize = 5;
            int length = db.Requirements.Where(z => z.ProjectId == ProjectId).ToList().Count();
            int pageNumber = (page ?? 1);

            var Req = db.Requirements.AsQueryable();
            if (rs != "")
            {
                Req = Req.Where(z => z.Modul.ModulName.ToLower().Contains(rs.ToLower()) || z.RequirementNumber.Contains(rs)
                || z.Modul.ModulNumber.Contains(rs)||z.RequimentType.Description.ToLower().Contains(rs) 
                || z.Description.ToLower().Contains(rs) || z.Level.ToString().Contains(rs)
                ).AsQueryable();
                length = Req.Where(z => z.ProjectId == ProjectId).ToList().Count();
            }
            var req = Req.Where(s => s.ProjectId == ProjectId).OrderBy(z => z.RequimentId).
                Select(r => new RequirmentViewModel()
                {
                    RequimentId = r.RequimentId,
                    Number = r.Modul.ModulNumber,
                    modulName = r.Modul.ModulName,
                    TypeReq = r.RequimentType.Description,
                    RequirementNumber = r.RequirementNumber,
                    Description = r.Description,
                    Level = r.Level,
                    CreationBy = r.CreationBy,
                    CreationDateStr = r.CreationDate.ToString(),
                    ModifiedBy = r.ModifiedBy,
                    ModifiedDateStr = r.ModifiedDate.ToString(),
                    length = length
                }).ToPagedList(pageNumber, pageSize);
            return Json(req, JsonRequestBehavior.AllowGet);

        }
        public JsonResult ListDataProject(int? page)
        {
            if (page == null)
            {
                page = 1;
            }

            int pageSize = 5;
            int length = db.Projects.ToList().Count();
            int pageNumber = (page ?? 1);
            var project = db.Projects
                .Include(p => p.Client)
                .Include(p => p.ProjectStatu)
                .OrderBy(s => s.ProjectId)
                .Select(p => new ProjectViewModel()
                {
                    ProjectId = p.ProjectId,
                    ClientId = p.ClientId,
                    ProjectStatusId = p.ProjectStatusId,
                    ProjectName = p.ProjectName,
                    Description = p.Description,
                    PIC = p.PIC,
                    MandaysEstimation = p.MandaysEstimation,
                    MandaysEstimationCal = p.MandaysEstimationCal,
                    CreationBy = p.CreationBy,
                    CreationDate = p.CreationDate,
                    ModifiedBy = p.ModifiedBy,
                    ModifiedDate = p.ModifiedDate,
                    ClientName = p.Client.Name,
                    ProjectStatusName = p.ProjectStatu.Description,
                    StartDatePlanStr = p.StartDatePlan.ToString(),
                    EndDatePlanStr = p.EndDatePlan.ToString(),
                    StartDateActualStr = p.StartDateActual.ToString(),
                    EndDateActualStr = p.EndDateActual.ToString(),
                    Length = length
                }).ToPagedList(pageNumber, pageSize);

            return Json(project, JsonRequestBehavior.AllowGet);

        }



        public JsonResult Search(ProjectViewModel valueSearch)
        {
            var project = db.Projects
                .Include(p => p.Client)
                .Include(p => p.ProjectStatu)
                .OrderBy(s => s.ProjectId)
                .Select(p => new ProjectViewModel()
                {
                    ProjectId = p.ProjectId,
                    ClientId = p.ClientId,
                    ProjectStatusId = p.ProjectStatusId,
                    ProjectName = p.ProjectName,
                    Description = p.Description,
                    PIC = p.PIC,
                    MandaysEstimation = p.MandaysEstimation,
                    MandaysEstimationCal = p.MandaysEstimationCal,
                    CreationBy = p.CreationBy,
                    CreationDate = p.CreationDate,
                    ModifiedBy = p.ModifiedBy,
                    ModifiedDate = p.ModifiedDate,
                    ClientName = p.Client.Name,
                    ProjectStatusName = p.ProjectStatu.Description,
                    StartDatePlanStr = p.StartDatePlan.Value.ToString(),
                    EndDatePlanStr = p.EndDatePlan.Value.ToString(),
                    StartDateActualStr = p.StartDateActual.Value.ToString(),
                    EndDateActualStr = p.EndDateActual.Value.ToString()
                    //EndDateActualStr = Convert.ToString(p.EndDateActual)
                });
            if (!(string.IsNullOrEmpty(valueSearch.ProjectName)))
            {
                project = project.Where(d => d.ProjectName.ToLower().Contains(valueSearch.ProjectName.ToLower())).OrderBy(s => s.ProjectId);
            }
            if (valueSearch.FromDatePlan != null && valueSearch.ToDatePlan != null)
            {
                DateTime FromDatePlan = DateTime.Parse(valueSearch.FromDatePlan);
                DateTime ToDatePlan = DateTime.Parse(valueSearch.ToDatePlan);
                project = project.Where(d => (d.StartDatePlan >= FromDatePlan) && (d.StartDatePlan <= ToDatePlan)).OrderBy(s => s.ProjectId);


            }
            if (valueSearch.FromDateActual != null && valueSearch.ToDateActual != null)
            {
                DateTime FromDateActual = DateTime.Parse(valueSearch.FromDateActual);
                DateTime ToDateActual = DateTime.Parse(valueSearch.ToDateActual);

                project = project.Where(d => d.StartDateActual.Value >= FromDateActual && d.StartDateActual.Value <= ToDateActual).OrderBy(s => s.ProjectId);
            }
            if (valueSearch.EndToDatePlan != null && valueSearch.EndFromDatePlan != null)
            {
                DateTime EndFromDatePlan = DateTime.Parse(valueSearch.EndFromDatePlan);
                DateTime EndToDatePlan = DateTime.Parse(valueSearch.EndToDatePlan);

                project = project.Where(d => d.EndDatePlan.Value >= EndFromDatePlan && d.EndDatePlan.Value <= EndToDatePlan).OrderBy(s => s.ProjectId);
            }
            if (valueSearch.EndToDateActual != null && valueSearch.EndFromDateActual != null)
            {
                DateTime EndFromDateActual = Convert.ToDateTime(valueSearch.EndFromDateActual);
                DateTime EndToDateActual = Convert.ToDateTime(valueSearch.EndToDateActual);
                project = project.Where(d => d.EndDateActual >= EndFromDateActual && d.EndDateActual <= EndToDateActual).OrderBy(s => s.ProjectId);
            }
            if (!(string.IsNullOrEmpty(valueSearch.PIC)))
            {
                project = project.Where(d => d.PIC.ToLower().Contains(valueSearch.PIC.ToLower())).OrderBy(s => s.ProjectId);
            }
            if (valueSearch.ProjectStatusId != null)
            {
                project = project.Where(d => d.ProjectStatusId == valueSearch.ProjectStatusId).OrderBy(s => s.ProjectId);
            }
            if (valueSearch.ClientId != null)
            {
                project = project.Where(d => d.ClientId == valueSearch.ClientId).OrderBy(s => s.ProjectId);
            }
            return Json(project.ToList(), JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetDatacRDById(int? id)
        {
            var data = db.RequirementDocuments.Find(id);
            RequirmentDocumentViewModel changeReDoc = new RequirmentDocumentViewModel()
            {
                DocumentId = data.DocumentId,
                RequimentId = data.RequimentId,
                DocumentTypeId = data.DocumentTypeId,
                DocumentName = data.DocumentName,
                FilePath = data.FilePath,
                Version = data.Version
            };

            return Json(changeReDoc, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDataDepById(int? id, int? idd)
        {
            var data = db.ModulDependencies.Find(idd);
            ModulDependencyViewModul dep = new ModulDependencyViewModul()
            {
                DependencyId = data.DependencyId,
                ModulId = data.ModulId,
                ModulName = data.Modul.ModulName,
                ModulNumber = data.Modul.ModulNumber
            };
            return Json(dep, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetByIdPro(int? id)
        {
            var proDoc = db.ModulDocuments.Find(id);
            ModulDocumentViewModel newpro = new ModulDocumentViewModel()
            {
                DocumentId=proDoc.DocumentId,
                ModulId=proDoc.ModulId,
                DocumentName = proDoc.DocumentName,
                FilePath = proDoc.FilePath,
                Version = proDoc.Version
            };
            return Json(newpro, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetByIdProdoc(int? id)
        {
            var proDoc = db.ProjectDocuments.Find(id);
            ProjectDocumentViewModel newpro = new ProjectDocumentViewModel()
            {
                ProjectId=proDoc.ProjectId,
                DocumentId = proDoc.DocumentId,
                DocumentName = proDoc.DocumentName,
                FilePath = proDoc.FilePath,
                Version = proDoc.Version
            };
            return Json(newpro, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetById(int? ProjectId)
        {
            var project = db.Projects.Find(ProjectId);
            ProjectViewModel newProject = new ProjectViewModel()
            {
                ClientId = project.ClientId,
                ClientName = project.Client.Name,
                CreationBy = project.CreationBy,
                CreationDate = project.CreationDate,
                Description = project.Description,
                EndDateActual = project.EndDateActual,
                EndDateActualStr = project.EndDateActual.ToString(),
                EndDatePlanStr = project.EndDatePlan.ToString(),
                MandaysEstimation = project.MandaysEstimation,
                ProjectName = project.ProjectName,
                MandaysEstimationCal = project.MandaysEstimationCal,
                ModifiedBy = project.ModifiedBy,
                PIC = project.PIC,
                ProjectId = project.ProjectId,
                ModifiedDate = project.ModifiedDate,
                ProjectStatusId = project.ProjectStatusId,
                StartDatePlan = project.StartDatePlan
            };
            return Json(newProject, JsonRequestBehavior.AllowGet);
        }

        /*     public JsonResult getBySearch(string searchProDoc)
             {
                 var hasil = db.Moduls.Select(z => new ModulViewModel() {
                     ModulName = z.ModulName,
                     ModulNumber = z.ModulNumber,
                     Description = z.Description,
                     PIC=z.PIC,
                     EndDateActual=z.Project.EndDateActual.ToString(),
                     EndDatePlan=z.Project.EndDatePlan.ToString(),
                     StartDateActual=z.Project.StartDateActual.ToString(),
                     StartDatePlan=z.Project.StartDatePlan.ToString()
                 });
                 hasil = hasil.Where(a => a.ModulName.ToLower().Contains(searchProDoc.ToLower()));
                 hasil = hasil.Where(a => a.Description.ToLower().Contains(searchProDoc.ToLower()));
                 hasil = hasil.Where(a => a.ModulNumber.Contains(searchProDoc));
                 hasil = hasil.Where(a => a.PIC.ToLower().Contains(searchProDoc.ToLower()));
                 return Json(hasil.ToList(), JsonRequestBehavior.AllowGet);
             }*/
        public JsonResult SaveToDb(ProjectViewModel valueEdited)
        {
            var projectEdited = db.Projects.Find(valueEdited.ProjectId);
            projectEdited.ProjectName = valueEdited.ProjectName;
            projectEdited.ProjectStatusId = valueEdited.ProjectStatusId;
            projectEdited.ClientId = valueEdited.ClientId;
            projectEdited.Description = valueEdited.Description;
            projectEdited.EndDateActual = valueEdited.EndDateActual;
            projectEdited.EndDatePlan = valueEdited.EndDatePlan;
            projectEdited.MandaysEstimation = valueEdited.MandaysEstimation;
            projectEdited.PIC = valueEdited.PIC;
            projectEdited.StartDateActual = valueEdited.StartDateActual;
            projectEdited.StartDatePlan = valueEdited.StartDatePlan;
            db.SaveChanges();
            return Json("Succes Edited for ProjectId " + valueEdited.ProjectId, JsonRequestBehavior.AllowGet);
        }


        public JsonResult ListDataModul(int ProjectId, int? page, string r)
        {
            if (page == null)
            {
                page = 1;
            }
            int pageSize = 5;
            int length = db.Moduls.Where(z => z.ProjectId == ProjectId).ToList().Count();
            var Modul = db.Moduls.Where(z => z.ProjectId == ProjectId).AsQueryable();
            if (r != "")
            {
                Modul = Modul.Where(z => z.ModulName.ToLower().Contains(r.ToLower()) || z.ModulNumber.Contains(r) ||
                z.Description.ToLower().Contains(r.ToLower()) || z.PIC.ToLower().Contains(r.ToLower())||
                z.ModulStatu.Description.ToLower().Contains(r.ToLower())|| z.MadaysEstimation.ToString().Contains(r)
                ).AsQueryable();
              

                length = Modul.Where(z => z.ProjectId == ProjectId).ToList().Count();
            }
            int pageNumber = (page ?? 1);
            var modul = Modul.Where(a => a.ProjectId == ProjectId).OrderBy(d => d.ModulId).
                Select(s => new ModulViewModel()
                {
                    ProjectId = s.ProjectId,
                    Description = s.Description,
                    MadaysEstimation = s.MadaysEstimation,
                    ModulId = s.ModulId,
                    ModulName = s.ModulName,
                    ModulNumber = s.ModulNumber,
                    ModulStatusId = s.ModulStatusId,
                    ParentModulId = s.ParentModulId,
                    PIC = s.PIC,
                    ModulStatusStr = s.ModulStatu.Description,
                    StartDatePlan = s.Project.StartDatePlan.ToString(),
                    StartDateActual = s.StartDateActual.ToString(),
                    EndDateActual = s.EndDateActual.ToString(),
                    EndDatePlan = s.Project.EndDatePlan.ToString(),
                    Lenght = length
                }).ToPagedList(pageNumber, pageSize);
            return Json(modul, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveModulDependency(int DependencyId,int ModulId,int DependToModul)
        {
            var dep = db.ModulDependencies.Find(DependencyId);
            dep.DependToModulId = DependToModul;
            dep.ModulId = ModulId;
            db.SaveChanges();
            return Json("Sukses Update " + dep.Modul.ModulName, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveProDocAdd(
            int typeDoc,
            int proId,
            string name,
            string files,
            string version
            )
        {
            ProjectDocument pro = new ProjectDocument()
            {
                DocumentName=name,
                DocumentTypeId=typeDoc,
                FilePath=files,
                ProjectId=proId,
                Version=version
                
            };
            db.ProjectDocuments.Add(pro);
            db.SaveChanges();
            var result = new jsonMessage();
            result.Message = "Add Success";
            result.Status = true;
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveModulProjectDocument(
            int DocId,
            int ProjectId,
            int Doc,
            string DocName,
            string txtFile,
            string txtVersion
            )
        {
            var result = new jsonMessage();
            var pro = db.ProjectDocuments.Find(DocId);
            pro.ProjectId = ProjectId;
            pro.DocumentName = DocName;
            pro.FilePath = txtFile;
            pro.Version = txtVersion;
            pro.DocumentTypeId = Doc;
            result.Status = true;
            result.Message = "Sukses";
            db.SaveChanges();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}
