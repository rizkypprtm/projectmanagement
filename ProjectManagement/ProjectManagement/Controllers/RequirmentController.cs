﻿using PagedList;
using ProjectManagement.Entities;
using ProjectManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


//Author             : Nispi A A
//Created date       : Selasa, 27/11/2018
//Modified by        : Nispi Abdul Aziz
//Date modified      : Rabu, 28/11/2018
//Additional         : EditReq,GetDataReqDocument,GetDataReqHistory,GetDataReqHistory,GetDataChangeReq
//Modified Date      : 3-12-2018


namespace ProjectManagement.Controllers
{
    public class RequirmentController : Controller
    {
        private ProjectManagementDBContext db = new ProjectManagementDBContext();
        // GET: Requirment
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult EditReq(int? requirmentId, string projectId, string reqNum, string number, string description, string type, string lvl)
        { 
            var data = db.Requirements.Find(requirmentId);
            data.ProjectId = Convert.ToInt32(projectId);
            data.RequirementNumber = reqNum;
            data.Description = description;
            //data.RequimentTypeId=
            data.Level = Convert.ToInt32(lvl);

            db.SaveChanges();
            return Json(data, JsonRequestBehavior.AllowGet);

        }

        public ActionResult EditReqDoc(
            int DocumentId,
            int RequimentId,
            string DocumentTypeId,
            string DocumentName,
            string FilePath,
            string Version)
        {
            var result = new jsonMessage();
            try
            {
                RequirementDocument reDoc = db.RequirementDocuments.Find(DocumentId);
                reDoc.RequimentId = RequimentId;
                reDoc.DocumentName = DocumentName;
                reDoc.FilePath = FilePath;
                reDoc.Version = Version;
                if (DocumentTypeId != "")
                {
                    reDoc.DocumentTypeId = Convert.ToInt32(DocumentTypeId);
                }
                result.Message = "Ubah Berhasil";
                result.Status = true;
                db.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                result.Message = e + "Permintaan Anda tidak bisa diproses. Coba lagi nanti.";
                result.Status = false;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
            //return Redirect("EditRe");
        }


        public JsonResult GetDataReqDocument(int? id,int? page)
        {
            if (page == null)
            {
                page = 1;
            }
            int pageSize = 5;
            int length = db.RequirementDocuments.Where(c => c.Requirement.RequimentId == id).ToList().Count();
            int pageNumber = (page ?? 1);
            var data = db.RequirementDocuments.Where(c => c.Requirement.RequimentId == id)
                .OrderBy(o => o.DocumentId)
                .Select(s => new RequirmentDocumentViewModel()
                {
                    DocumentId = s.DocumentId,
                    RequimentId = s.RequimentId,
                    documentType = s.DocumentType.Description,
                    DocumentName = s.DocumentName,
                    requirmentNumber=s.Requirement.RequirementNumber,
                    FilePath = s.FilePath,
                    Version = s.Version,
                    Length=length
                }).ToPagedList(pageNumber, pageSize);

            return Json(data, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetDataReqHistory(int? id,int? page)
        {
            if (page == null)
            {
                page = 1;
            }
            int pageSize = 5;
            int length = db.RequirementHistories.Where(c => c.Requirement.RequimentId == id).ToList().Count();
            int pageNumber = (page ?? 1);
            var data = db.RequirementHistories.Where(c => c.Requirement.RequimentId == id)
                .OrderBy(o => o.HistoryId)
                .Select(s => new RequirmentHistoryViewModel()
                {
                    RequimentId = s.RequimentId,
                    RequirementNumber = s.RequirementNumber,
                    CreationBy = s.CreationBy,
                    Description=s.Description,
                    CreationDate=s.CreationDate,
                    ModifiedBy=s.ModifiedBy,
                    ModifiedDate=s.ModifiedDate,
                    Level=s.Level,
                    Length=length
                }).ToPagedList(pageNumber, pageSize);
          
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDataChangeReq(int? id,int? page)
        {
            if (page == null)
            {
                page = 1;
            }

            int pageSize = 5;
            int length = db.ChangeRequests.Where(c => c.Requirement.RequimentId == id).ToList().Count();
            int pageNumber = (page ?? 1);
            var data = db.ChangeRequests.Where(c => c.Requirement.RequimentId == id)
                .OrderBy(o => o.ChangeRequestId)
                .Select(s => new ChangeRequesViewModel()
                {
                    ChangeRequestId = s.ChangeRequestId,
                    RequimentId = s.RequimentId,
                    ProjectName = s.Requirement.Project.ProjectName,
                    ModulName = s.Requirement.Modul.ModulName,
                    Description = s.Description,
                    ChangeRequestNumber = s.ChangeRequestNumber,
                    ChangeRequestDate = s.ChangeRequestDate,
                    MandaysEstimation = s.MandaysEstimation,
                    lenght=length
                }).ToPagedList(pageNumber, pageSize);
            // int idx = 0;
            /*foreach (var x in dataMD)
            {
                var tamp = db.ChangeRequests.Where(z=>z.RequimentId==id);
                dataMD[idx].ProjectName = tamp;
                dataMD[idx].Description = tamp.Description;
                dataMD[idx].Level = tamp.Level;
                dataMD[idx].CreationBy = tamp.CreationBy;
                dataMD[idx].CreationDate = tamp.CreationDate;
                dataMD[idx].ModifiedBy = tamp.ModifiedBy;
                dataMD[idx].ModifiedDate = tamp.ModifiedDate;

                idx += 1;
            }*/
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDataRDById(int? id)
        {
            var data = db.RequirementDocuments.Find(id);
            RequirmentDocumentViewModel changeReDoc = new RequirmentDocumentViewModel()
            {
                DocumentId = data.DocumentId,
                RequimentId = data.RequimentId,
                DocumentTypeId = data.DocumentTypeId,
                DocumentName = data.DocumentName,
                FilePath = data.FilePath,
                Version = data.Version
            };

            return Json(changeReDoc, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int? Id)
        {
            ViewBag.RequirmentId = Id;
            Requirement req = db.Requirements.Find(Id);
            //ViewBag.ParentModulId = new SelectList(db.Moduls.Where(p => p.ProjectId == modul.ProjectId), "ModulId", "ModulName");
            ViewBag.RequimentTypeId = new SelectList(db.RequimentTypes,"RequimentTypeId", "Description");
            ViewBag.ModulNumber = new SelectList(db.Moduls.Where(p=>p.ModulId==req.ModulId), "ModulId", "ModulNumber");
            ViewBag.ProjectId = new SelectList(db.Projects.Where(p=>p.ProjectId==req.ProjectId), "ProjectId", "ProjectName");
            ViewBag.DocumentTypeId = new SelectList(db.DocumentTypes, "DocumentTypeId", "Description");
            //ViewBag.RequimentTypeId = new SelectList("RequimentTypeId", "Description");
            return View(req);
        }
        public JsonResult EditViewJs(int? id)
        {
            var result = new jsonMessage();
            var dataReq = db.Requirements.Where(p=>p.RequimentId==id).Select(s=> new RequirmentViewModel() {
                ProjectId=s.ProjectId,
                Description = s.Description,
                Level = s.Level,
                RequimentTypeId = s.RequimentTypeId,
                RequirementNumber = s.RequirementNumber,
                ProjectName=s.Project.ProjectName,
                Number=s.Modul.ModulNumber
            }).ToList();
            //result.Status = true;
            return Json(dataReq, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDataRDocById(int? id)
        {
            var data = db.RequirementDocuments.Find(id);
            RequirmentDocumentViewModel changeReDoc = new RequirmentDocumentViewModel()
            {
                DocumentId = data.DocumentId,
                RequimentId = data.RequimentId,
                DocumentTypeId = data.DocumentTypeId,
                DocumentName = data.DocumentName,
                FilePath = data.FilePath,
                Version = data.Version
            };

            return Json(changeReDoc, JsonRequestBehavior.AllowGet);
        }
    }
}