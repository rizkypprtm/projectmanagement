﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjectManagement.Entities;

namespace ProjectManagement.Controllers
{
    public class ProjectDocumentController : Controller
    {
        private ProjectManagementDBContext db = new ProjectManagementDBContext();

        // GET: ProjectDocument
        public ActionResult Index()
        {
            var projectDocuments = db.ProjectDocuments.Include(p => p.DocumentType).Include(p => p.Project);
            ViewBag.DocumentTypeId = new SelectList(db.DocumentTypes, "DocumentTypeId", "Description");
            
            return View(projectDocuments.ToList());
        }

        [HttpPost]
        public ActionResult AddEdit(string ProjectDocumentId, string DocumentName, string FilePath, string Version, int? id)
        {
            if(id != null)
            {
                ProjectDocument projectDocument = db.ProjectDocuments.Find(id);
                if (projectDocument == null)
                {
                    return HttpNotFound();
                }
                projectDocument.DocumentName = DocumentName;
                projectDocument.DocumentTypeId = Convert.ToInt32(ProjectDocumentId);
                projectDocument.FilePath = FilePath;
                projectDocument.Version = Version;

                db.Entry(projectDocument).State = EntityState.Modified;
                db.SaveChanges();

                ViewBag.DocumentTypeId = new SelectList(db.DocumentTypes, "DocumentTypeId", "Description", projectDocument.DocumentTypeId);

                return Json(new
                {
                    msg = "Successfully Edit Project Document"
                });
            }
            else
            {
                ProjectDocument newProjectDocument = new ProjectDocument()
                {
                    DocumentName = DocumentName,
                    FilePath = FilePath,
                    Version = Version
                };
                db.ProjectDocuments.Add(newProjectDocument);
                db.SaveChanges();
                //return Json(new
                //{
                //    msg = "Successfully Added New Project Document"
                //});
                return RedirectToAction("Index");
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
