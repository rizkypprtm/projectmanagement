﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
// author       :Fauzi
// Created date : 23-11-2018
// Modified by  :Fauzi
// Date Modified: 05-12-2018
// Addtional    : add attribute length

namespace ProjectManagement.Models
{
    public class ProjectTimeLineViewModel
    {
        public int TimelineId { get; set; }

        public int? ProjectScheduleId { get; set; }

        public int? ProjectId { get; set; }

        public int? ModulId { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
        public string StartDateStr { get; set; }
        public string EndDateStr { get; set; }


        public string ProjectName { get; set; }

        public string ModulName { get; set; }
        //attribute tambahan
        public int Length { get; set; }
        public string ModulNumber { get; set; }
        public string Version { get; set; }

        public string SD { get; set; }
        public string ED { get; set; }
    }
}