﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagement.Models
{
    public class ChangeRequestDocumentViewModel
    {
        public int DocumentId { get; set; }

        public int? ChangeRequestId { get; set; }

        public string ModulNumber { get; set; }

        public string ModulName { get; set; }

        public string RequirementNumber { get; set; }

        public string RequirementDescription { get; set; }

        public string ProjectName { get; set; }

        public string Description { get; set; }

        public int? Level { get; set; }

        public string CreationBy { get; set; }

        public DateTime? CreationDate { get; set; }

        public string StrCreationDate { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string StrModifiedDate { get; set; }

        public int? DocumentTypeId { get; set; }

        public string DocumentName { get; set; }

        public string FilePath { get; set; }

        public string Version { get; set; }
        //attribute tambahan
        public int lenght { get; set; }
    }
}