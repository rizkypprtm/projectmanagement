﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
// author       :Fauzi
// Created date : 23-11-2018
// Modified by  :
// Date Modified: 
// Addtional    : 


namespace ProjectManagement.Models
{
    public class ModulViewModel
    {
        public int ModulId { get; set; }

        public int? ParentModulId { get; set; }

        public int? ProjectId { get; set; }   

        public int? ModulStatusId { get; set; }

        public string ModulStatusStr { get; set; }
        public string StartDatePlan { get; set; }
        public string StartDateActual { get; set; }
        public string EndDatePlan { get; set; }
        public string EndDateActual { get; set; }

        public string ModulName { get; set; }

        public string Description { get; set; }

        public string PIC { get; set; }

        public string ModulNumber { get; set; }

        public decimal? MadaysEstimation { get; set; }
        public int Lenght { get; set; }
        public string ProjectName { get; set; }
        public string ParentModul { get; set; }
    }
}