﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagement.Models
{
    public class ProjectDocumentViewModel
    {
        public int DocumentId { get; set; }
        public int? ProjectId { get; set; }
        
        public string DocumentName { get; set; }

        public string FilePath { get; set; }
        
        public string Version { get; set; }
        
        public string DocumentType { get; set; }
        public int Length { get; set; }

    }
}