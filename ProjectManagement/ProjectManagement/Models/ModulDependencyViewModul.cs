﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagement.Models
{
    public class ModulDependencyViewModul
    {
        public int DependencyId { get; set; }

        public int? ModulId { get; set; }

        public int? DependToModulId { get; set; }
        public string ModulName { get; set; }
        public string ModulNumber { get; set; }
        public int Length { get; set; }
    }
}