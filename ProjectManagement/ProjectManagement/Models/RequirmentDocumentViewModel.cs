﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

// author       :Nispi Abdul Aziz
// Created date : 28-11-2018
// Modified by  :
// Date Modified: 
// Addtional    :


namespace ProjectManagement.Models
{
    public class RequirmentDocumentViewModel
    {
        public int DocumentId { get; set; }

        public int? RequimentId { get; set; }

        public int? DocumentTypeId { get; set; }
        
        public string DocumentName { get; set; }

        public string FilePath { get; set; }
        
        public string Version { get; set; }

        public string documentType { get; set; }

        public string requirmentNumber { get; set; }
        public int Length { get; set; }
    }
}