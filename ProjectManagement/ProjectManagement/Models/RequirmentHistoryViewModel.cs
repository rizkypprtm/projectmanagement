﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

// author       : Nispi Abdul Aziz
// Created date : 28-11-2018
// Modified by  :
// Date Modified: 
// Addtional    : 

namespace ProjectManagement.Models
{
    public class RequirmentHistoryViewModel
    {
        public int HistoryId { get; set; }

        public int RequimentId { get; set; }
        
        public string RequirementNumber { get; set; }

        public string Description { get; set; }

        public int? Level { get; set; }
        public string CreationBy { get; set; }

        public DateTime? CreationDate { get; set; }
        
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }
        public int Length { get; set; }
    }
}