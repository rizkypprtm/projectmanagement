﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
// author       :Fauzi
// Created date : 23-11-2018
// Modified by  :Nispi Abdul Aziz
// Date Modified: 28/11/2018
// Addtional    : Menambah Project Name, Number

// author       :Fauzi
// Created date : 23-11-2018
// Modified by  :Fauzi
// Date Modified: 05/12/2018
// Addtional    : Menambah length

namespace ProjectManagement.Models
{
    public class RequirmentViewModel
    {
        public int RequimentId { get; set; }

        public int? ParentRequimentId { get; set; }

        public int? ProjectId { get; set; }

        public int? ModulId { get; set; }

        public int? RequimentTypeId { get; set; }

        public string RequirementNumber { get; set; }

        public string Description { get; set; }

        public string Details { get; set; }

        public int? Level { get; set; }

        public string CreationBy { get; set; }

        public DateTime? CreationDate { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }
        public string RequiermentType { get; set; }
        public string ModifiedDateStr { get; set; }
        public string CreationDateStr { get; set; }

        public string ProjectName { get; set; }
        public string Number { get; set; }
        public string modulName { get; set; }
        public string TypeReq { get; set; }
        public int length { get; set; }
    }
}