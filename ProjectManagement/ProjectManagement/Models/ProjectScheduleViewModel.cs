﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagement.Models
{
    public class ProjectScheduleViewModel
    {
        public int ProjectScheduleId { get; set; }

        public int? ProjectId { get; set; }
        
        public string Version { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }
        
        public string ProjectName { get; set; }
        public int Lenght { get; set; }
    }
}