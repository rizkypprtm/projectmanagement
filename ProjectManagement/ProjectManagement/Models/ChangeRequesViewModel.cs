﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
// author       :Fauzi
// Created date : 23-11-2018
// Modified by  :Nispi Abdul Aziz
// Date Modified: 28/11/2018
// Addtional    : Menambah Project Name, Modul Name

// author       :Fauzi
// Created date : 23-11-2018
// Modified by  :Fauzi
// Date Modified: 05-12-2018
// Addtional    : add attribute length

namespace ProjectManagement.Models
{
    public class ChangeRequesViewModel
    {
        public int ChangeRequestId { get; set; }

        public int? RequimentId { get; set; }

        public string ChangeRequestNumber { get; set; }

        public string Description { get; set; }

        public DateTime? ChangeRequestDate { get; set; }

        public string StrChangeRequestDate { get; set; }

        public decimal? MandaysEstimation { get; set; }

        public string ProjectName { get; set; }

        public string ModulName { get; set; }

        public string modulNumber { get; set; }

        public string reqnumber { get; set; }
        // atrribute tambahan
        public string RequirementRequestNumber { get; set; }
        public int lenght { get; set; }
        public string RequirementDescription { get; set; }
    }
}