﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
// author       :Fauzi
// Created date : 23-11-2018
// Modified by  :
// Date Modified: 
// Addtional    : 

namespace ProjectManagement.Models
{
    public class DocumentTypeViewModel
    {
        public int DocumentTypeId { get; set; }

        public string Description { get; set; }
    }
}