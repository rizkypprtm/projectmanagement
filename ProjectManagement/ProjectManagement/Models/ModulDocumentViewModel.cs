﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

// author       :Fauzi
// Created date : 23-11-2018
// Modified by  :Fauzi
// Date Modified: 05-12-2018
// Addtional    : add attribute length

namespace ProjectManagement.Models
{
    public class ModulDocumentViewModel
    {
        public int DocumentId { get; set; }

        public int? ModulId { get; set; }

        public int? DocumentTypeId { get; set; }

        public string DocumentName { get; set; }

        public string FilePath { get; set; }

        public string Version { get; set; }

        public string DocumentType { get; set; }
        // attribute tambahan
        public int length { get; set; }

    }
}