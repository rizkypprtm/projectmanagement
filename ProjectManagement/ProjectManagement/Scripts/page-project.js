﻿//Load Data in Table when documents is ready
$(document).ready(function () {
    loadData();
});
//Load Data function
function loadData(p) {
    if (p == null) {
        p = 1;
    }
    $.ajax({
        url: "/Project/ListDataProject?page=" + p,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            var html = '';
            var btn = '';
            var c = 0;
            var ctr = 0;
            $.each(result, function (key, item) {
                html += '<tr>';
                html += '<td>' + item.ProjectName + '</td>';
                html += '<td>' + item.ClientName + '</td>';
                html += '<td>' + item.Description + '</td>';
                html += '<td>' + item.PIC + '</td>';
                html += '<td>' + item.ProjectStatusName + '</td>';
                html += '<td>' +
                                        'Start Date Plan____:  ' + item.StartDatePlanStr + '</br>' +
                                        'End Date Plan______:  ' + item.EndDatePlanStr + '</br>' +
                                        'Start Date Actual__:  ' + item.StartDateActualStr + '</br>' +
                                        'End Date Actual____:  ' + item.EndDateActualStr + '</br>' +
                                     '</td>';
                html += '<td><button onclick="return GetById(' + item.ProjectId + ')" class="btn btn-warning" >EditOk</button></td>';
                html += '</tr>';
                c = item.Length;
                ctr++;
            });
            $('.tbody').html(html);
            var t = 1;
            for (var i = 0; i < c; i += 10) {
                btn += '<button onclick="return loadData(' + t + ')" class="btn btn-info" >' + t + '</button>';
                t++;
            }
            $('#buttonPage').html(btn);
            $('#pagePosition').html('Anda berada di halaman : ' + p + ' dari ' + (t - 1));
            $('#jumlahData').html('Jumlah Data : ' + ctr + ' dari ' + c);
        }
    });
}


function Search() {
    var valueSearch = {
        ProjectName: $('#ProjectName').val(),
        FromDatePlan: $('#FromDatePlan').val(),
        ToDatePlan: $('#ToDatePlan').val(),
        EndFromDatePlan: $('#EndFromDatePlan').val(),
        EndToDatePlan: $('#EndToDatePlan').val(),
        ProjectStatusId: $('#ProjectStatusId').val(),
        ProjectName: $('#ProjectName').val(),
        ClientId: $('#ClientId').val(),
        FromDateActual: $('#FromDateActual').val(),
        ToDateActual: $('#ToDateActual').val(),
        PIC: $('#PIC').val(),
        EndFromDateActual: $('#EndFromDateActual').val(),
        EndToDateActual: $('#EndToDateActual').val()

    };
    $.ajax({
        url: "/Project/Search",
        data: JSON.stringify(valueSearch),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            var html = '';
            var btn = '';
            var c = 0;
            var ctr = 0;
            $.each(result, function (key, item) {
                html += '<tr>';
                html += '<td>' + item.ProjectName + '</td>';
                html += '<td>' + item.ClientName + '</td>';
                html += '<td>' + item.Description + '</td>';
                html += '<td>' + item.PIC + '</td>';
                html += '<td>' + item.ProjectStatusName + '</td>';
                html += '<td>' +
                                        'Start Date Plan____:  ' + item.StartDatePlanStr + '</br>' +
                                        'End Date Plan______:  ' + item.EndDatePlanStr + '</br>' +
                                        'Start Date Actual__:  ' + item.StartDateActualStr + '</br>' +
                                        'End Date Actual____:  ' + item.EndDateActualStr + '</br>' +
                                     '</td>';
                html += '<td><button onclick="return edit(' + item.ProjectId + ')" class="btn btn-warning" >Edit</button></td>';
                html += '</tr>';
                ctr++;
            });
            $('.tbody').html(html);
            $('#buttonPage').html('');
            $('#pagePosition').html('');
            $('#jumlahData').html('');
            $('.tbody').html(html);
            $('#ProjectName').val('');
            $('#FromDatePlan').val('');
            $('#ToDatePlan').val('');
            $('#EndFromDatePlan').val('');
            $('#EndToDatePlan').val('');
            $('#ProjectStatusId').val('');
            $('#ProjectName').val('');
            $('#ClientId').val('');
            $('#FromDateActual').val('');
            $('#ToDateActual').val('');
            $('#PIC').val('');
            $('#EndFromDateActual').val('');
            $('#EndToDateActual').val('');
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}

function GetById(IdProject) {
    $.ajax({
        url: "/Project/GetById/" + ProjectId,
        typr: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#ProjectId').val(result.ClientId);
            $('#ProjectName').val(result.Name);
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
    return false;
}